from django.test import TestCase
from django.urls import reverse

from users.tests.factories import UserFactory
from transactions.tests.factories import TransactionFactory


class TransactionAdminTest(TestCase):

    def setUp(self):
        self.user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login(self.user)
        self.trans = TransactionFactory()

    def test_changelist_view(self):
        '''Assert transactions change list view loads well'''

        url = reverse("admin:%s_%s_changelist" % (self.trans._meta.app_label, self.trans._meta.model_name))
        page = self.client.get(url)
        self.assertEqual(page.status_code, 200)

    def test_change_view(self):
        '''Assert transactions change view page opens successfully'''

        url = reverse("admin:%s_%s_change" % (self.trans._meta.app_label, self.trans._meta.model_name), args=(self.trans.pk,))
        page = self.client.get(url)
        self.assertEqual(page.status_code, 200)