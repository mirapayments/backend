from django.test import SimpleTestCase
from django.urls import resolve

from transactions import views


class TransactionUrlsResolvesToViewTest(SimpleTestCase):

    def test_transaction_list_create_url_resolves_to_view(self):
        '''assert that list create transactions url resolves to the right view class'''

        found = resolve('/transactions/123344/')
        # use func.view_class to get the class for the view
        self.assertEqual(found.func.view_class, views.ListTransactionView)

    def test_transaction_list_retrieve_url_resolves_to_view(self):
        '''assert that retrieve transactions url resolves to the right view class'''

        found = resolve('/transactions/transaction/2/')
        self.assertEqual(found.func.view_class, views.RetrieveTransactionView)

    def test_transaction_export_url_resolves_to_view(self):
        '''assert that export transaction url resolves to the right view class'''

        found = resolve('/transactions/export/123456/')
        self.assertEqual(found.func.view_class, views.ExportTransactionView)

    def test_pamentlink_transaction_create_url_resolves_to_view(self):
        '''assert that paymentlink transaction create url resolves to the right view class'''

        found = resolve('/transactions/create/123456/')
        self.assertEqual(found.func.view_class, views.CreatePaymentLinkTransactionView)