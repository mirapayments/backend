from django.db import models


class Customer(models.Model):
    '''
    Store's Customer's data from a transaction or as input by an account owner

    - full_name: the full name of the customer
    - email: the email of the customer
    - phone: the phone number of customer
    - blocked: whether customer has been blocked or not
    - account: the account where info is tied to
    - created_at: time when customer was created
    - updated_at: time when customer detail was updated

    '''
    full_name = models.CharField(max_length=200, default='', help_text='e.g. Miracle Alex')
    email = models.EmailField(max_length=200, help_text='e.g. example@gmail.com')
    phone = models.CharField(max_length=50, default='', help_text='e.g. +23480 2604 3569')
    blocked = models.BooleanField(default=False)
    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.email
