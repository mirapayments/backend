import factory


class CustomerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'customers.Customer'

    full_name = factory.Sequence(lambda n: 'full name {}'.format(n))
    email = factory.Sequence(lambda n: 'user{}@gmail.com'.format(n))
    phone = factory.Sequence(lambda n: '0801234567{}'.format(n))
    blocked = False
    account = factory.SubFactory('accounts.tests.factories.AccountFactory')
