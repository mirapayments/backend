from django.contrib import admin

from helpers.handlers import ExportCsvMixin
from accounts.models import Compliance, Account, TestAccount


@admin.register(Compliance)
class ComplianceAdmin(admin.ModelAdmin):
    pass

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = ['account_number', 'account_type', 'name', 'setup_complete', 'balance', 'is_active', 'created_at', 'updated_at']
    list_filter = ('account_type', 'is_active', 'setup_complete')
    actions = ["export_items_to_csv"]
    readonly_fields = ['balance']
    search_fields = ('account_number',)
    date_hierarchy = 'created_at'


@admin.register(TestAccount)
class TestAccountAdmin(admin.ModelAdmin):
    list_display = ['account_number', 'account_type', 'name', 'balance', 'created_at', 'updated_at']

    
