from django.test import SimpleTestCase
from django.urls import resolve

from payments import views


class PaymentUrlsResolvesToViewTest(SimpleTestCase):

    def test_url_resolves_to_list_create_payments_link_view(self):
        '''assert that list create payments-link url resolves to the right view class'''

        found = resolve('/payments/payment-link/123344/')
        # use func.view_class to get the class for the view
        self.assertEqual(found.func.view_class, views.CreateListPaymentLinkView)

    def test_url_resolves_to_retrieve_delete_update_payments_link_view(self):
        '''assert that retrieve, delete update url resolves to the right view class'''

        found = resolve('/payments/payment-link/2/1234444/')
        self.assertEqual(found.func.view_class, views.RetrieveUpdateDeletePaymentLinkView)

    def test_url_resolves_to_code_retrieve_payments_link_view(self):
        '''assert that code retrieve payment url resolves to the right view class'''

        found = resolve('/payments/payment-link/code/actualcode/')
        self.assertEqual(found.func.view_class, views.CodeRetrievePaymentLink)

    def test_url_resolves_to_toggle_payments_link_view(self):
        '''assert that toggle payment url resolves to the right view class'''

        found = resolve('/payments/payment-link/toggle/1/2333344433/')
        self.assertEqual(found.func.view_class, views.TogglePaymentLink)