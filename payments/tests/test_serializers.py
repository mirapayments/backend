from rest_framework.test import APITestCase

from accounts.tests.factories import  AccountFactory
from payments.serializers import PaymentLinkSerializer

class PaymentLinkSerializerTest(APITestCase):

    def test_payment_link_create_serializer(self):
        '''Test Payment Link serializer :isvalid() and :create() methods'''

        data = {
            'name': 'new payment link',
            'description': 'to collect plenty money',
            'payment_type': 'Single',
            'amount_currency': 'USD',
            'amount': 100.0
        }

        paylink_serializer = PaymentLinkSerializer(data=data)

        # assert serilizers checks validity correctly
        self.assertTrue(paylink_serializer.is_valid())       

        # assert :create() method works as expected
        account = AccountFactory()
        acc_instance = paylink_serializer.save(account=account)
        self.assertEqual(acc_instance.name, data['name'])