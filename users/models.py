from django.db import models
from django.contrib.auth.models import AbstractUser

from users.managers import CustomUserManager


class User(AbstractUser):
    '''
    The model to store users

    - username: is None (we don't authenticate with username)
    - email: the user email, unique 
    - phone: the user phone number
    - email_verified: whether email is verified or not
    - send_notifications: whether notifications to be sent to user or not
    - accounts: the accounts a user belongs to
    - hear_about_us: where the user heard about us
    - country: the country of user
    - updated_at: time user model was updated
    '''
    username = None
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=25, default='', blank=True)
    email_verified = models.BooleanField(default=False)
    send_notifications = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)
    accounts = models.ManyToManyField('accounts.Account')
    hear_about_us = models.CharField(max_length=250, default='')
    country = models.CharField(max_length=100, default='', blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    def __str__(self):
        return self.email

class Role(models.Model):
    '''
    The model to store user roles

    - role: the role choices for a user
    - account: the account this role belongs to
    - user: the account this role belongs to
    '''    
    OWNER = 'Owner'
    ADMIN = 'Admin'
    OPERATIONS = 'Operations'
    SUPPORT = 'Support'
    DEVELOPER = 'Developer'
    ROLE_CHOICES = (
        (OWNER, OWNER),
        (ADMIN, ADMIN),
        (OPERATIONS, OPERATIONS),
        (SUPPORT, SUPPORT),
        (DEVELOPER, DEVELOPER),
    )
    role = models.CharField(max_length=100, choices=ROLE_CHOICES, blank=True)
    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE, null=True)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, null=True)