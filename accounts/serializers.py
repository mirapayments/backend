from transactions.serializers import TransactionSerializer
from transactions.models import Transaction
from django.conf import settings
from django.db import transaction

from rest_framework import serializers
from djmoney.contrib.django_rest_framework import MoneyField
from djmoney.money import Money
from knox.models import AuthToken

from accounts.models import Account, Compliance, TestAccount
from users.models import Role


class AccountSerializer(serializers.ModelSerializer):
    balance = MoneyField(max_digits=14, decimal_places=2, min_value=0)

    class Meta:
        model = Account
        fields = ['account_number', 'name','balance','is_active','balance_currency','setup_complete','account_type','fees_payer']


class ComplianceSerializer(serializers.ModelSerializer):
    identity_type = serializers.ChoiceField(choices=Compliance.IDENTITY_CHOICES)
    setup_complete = serializers.BooleanField(source='account.setup_complete', read_only=True)

    class Meta:
        model = Compliance
        fields = '__all__'

    def update(self, instance, validated_data):
        # if new file available, delete old one
        if 'identity_file' in validated_data:
            instance.identity_file.delete() 
        instance = super().update(instance, validated_data) 

        # check that all fields were filled, if True: activate account
        field_names = [x.name for x in instance._meta.fields]
        values = [getattr(instance, field, None) for field in field_names]
   
        if all(values):
            instance.account.setup_complete = True
            instance.account.save(update_fields=['setup_complete'])
            TestAccount.objects.filter(account_number=instance.account.account_number).update(setup_complete=True)
        return instance  


class NewAccountSerializer(serializers.Serializer):
    name = serializers.CharField()
    balance_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES)
    account_type = serializers.ChoiceField(choices=Account.ACCOUNT_CHOICES, required=True)

    def create(self, validated_data):
        balance_currency = validated_data.pop('balance_currency')
        validated_data['balance'] = Money(0.0, balance_currency) # setup Money instance for balance field
        account = None
        with transaction.atomic():
            # Create account, add user to account, create role, compliance and tokens for ext api calls
            account = Account.objects.create(**validated_data)
            self.context['request'].user.accounts.add(account)
            AuthToken.objects.create(user=self.context['request'].user, account=account)
            Role.objects.create(role=Role.OWNER, account=account, user=self.context['request'].user)
            Compliance.objects.create(account=account)

        return account

class BalanceSerializer(serializers.Serializer):
    balance = MoneyField(max_digits=14, decimal_places=2, min_value=0)
    balance_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES)
    transactions = serializers.SerializerMethodField()

    def get_transactions(self, obj):
        trans = obj.transaction_set.all()
        # TODO: fix this filter
        topup = TransactionSerializer(trans.exclude(transaction_type=Transaction.BANK_TRANSFER), many=True).data
        payouts = TransactionSerializer(trans.filter(transaction_type=Transaction.BANK_TRANSFER), many=True).data
        return {
            'topup': topup,
            'payouts': payouts
        }