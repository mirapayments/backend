from django.contrib.auth.password_validation import validate_password, get_password_validators
from django.conf import settings
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.db import transaction

from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from djmoney.money import Money

from accounts.models import Account, Compliance
from accounts.serializers import AccountSerializer
from users.models import User, Role
from users.signals import pre_password_reset, post_password_reset
from helpers.mailers import send_user_activation_mail, invite_existing_user
from helpers.utils import validate_token
from knox.models import AuthToken



class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()
    account_number = serializers.CharField(required=False)

class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True,
                validators=[UniqueValidator(queryset=User.objects.all(),
                message='User with this email already exists', lookup='iexact')])
    password = serializers.CharField(min_length=8, write_only=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    account_name = serializers.CharField(write_only=True, required=True)
    phone = serializers.CharField(read_only=True)
    accounts = serializers.SerializerMethodField(read_only=True)
    hear_about_us = serializers.CharField(required=False, write_only=True)
    currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES, write_only=True)
    account_type = serializers.ChoiceField(choices=Account.ACCOUNT_CHOICES, write_only=True)
    country = serializers.CharField(read_only=True)

    def validate_password(self, value):
        try:
            validate_password(
                password=value,
                password_validators=get_password_validators(settings.AUTH_PASSWORD_VALIDATORS)
            )
            return value
        except Exception as e:
            raise serializers.ValidationError(e) 

    def get_accounts(self, obj):
        accounts = obj.accounts.all()
        # this is currently an overkill
        # data = AccountSerializer(accounts, many=True).data

        return [{'account_number': acc.account_number, 'name': acc.name} for acc in accounts]

    def to_representation(self, instance):
        data = super().to_representation(instance)  
        if self.context.get('account'):
            role = Role.objects.filter(account=self.context['account'], user=instance).first()
            data['role'] = role.role if role else None
        return data  


    
    class Meta:
        model = User
        fields = ['id', 'email', 'password', 'first_name', 'last_name','full_name','phone', 'hear_about_us','account_name', 
                'last_login', 'is_active', 'currency', 'account_type','country', 'accounts']

    def create(self, validated_data):
        name = validated_data.pop('account_name')
        account_type = validated_data.pop('account_type')
        currency = validated_data.pop('currency')
        balance = Money(0.0, currency)
        user = None

        # create user, compliance, account and user to account
        # create token for external auth and role for user in account
        with transaction.atomic():
            user = User.objects.create(**validated_data)
            account = Account.objects.create(name=name, balance=balance, account_type=account_type)
            user.accounts.add(account)
            AuthToken.objects.create(user=user, account=account)
            Compliance.objects.create(account=account)
            Role.objects.create(role=Role.OWNER, account=account, user=user)

        return user

class UserUpdateSerializer(serializers.Serializer):
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    phone = serializers.CharField(required=False)
    country = serializers.CharField(required=False)
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'phone', 'country']

    def update(self, instance, validated_data):       
        for attr, value in validated_data.items():
            setattr(instance, attr, value) 
       
        instance.save()
        return instance    


class UserInvitationSerializer(serializers.Serializer):
    email = serializers.EmailField()
    role = serializers.ChoiceField(choices=Role.ROLE_CHOICES)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        email = attrs.get('email')
        role = attrs.get('role')
        account = self.context.get('account')

        #################################################################
        # DO NOT MODIFY IF YOU DON'T KNOW WHAT YOU'RE DOING!!!
        #################################################################

        # only owners can invite another Owner
        # inviter_role = Role.objects.filter(account=account, user=self.context['request'].user).first()
        # if (role == Role.OWNER) and not (inviter_role and inviter_role.role == Role.OWNER):
        #     raise serializers.ValidationError('Only an account owner can invite another owner')

        if role == Role.OWNER:
            raise serializers.ValidationError('Cannot invite an owner')

        # handle case when invited user already exists
        self.user = User.objects.filter(email__iexact=email).first()
        if self.user: #if user exists
            if account in self.user.accounts.all():  # and user is a member of the account
                if self.user.is_active:  # and user is already active
                    raise serializers.ValidationError('Cannot invite already activated user')
                else:
                    # resend mail when user is inactive, but has never logged in (prevent admin deactivated user from activating themeselves) 
                    if self.user.last_login is None:  
                        send_user_activation_mail(self.user, account.name)
                    else:
                        # raise error when inviting a deactivated user
                        raise serializers.ValidationError('Cannot invite a deactivated user, please contact us')

            else: # inviting existing user that's not a member of the account
                if self.user.is_active:  # if user is active, add user to account
                    self.user.accounts.add(account)
                    Role.objects.create(role=role, account=account, user=self.user)
                    invite_existing_user(self.user.email, account.name)
                else: # raise error if user is inactive
                    raise serializers.ValidationError('Cannot invite a deactivated user, please contact us')
        #################################################################
        return attrs


    def invite(self):
        account = self.context['account']
        role = self.validated_data.pop('role')

        if not self.user: # if user doesn't exist, create user, add to account, assign role and invite user
            with transaction.atomic():
                self.user = User.objects.create(**self.validated_data, is_active=False)
                self.user.accounts.add(account)
                Role.objects.create(role=role, account=account, user=self.user)

            send_user_activation_mail(self.user, account.name)
        return self.user


class TokenValidateMixin:
    def validate(self, data):
        token = data.get('token')
        uid = data.get('uid')

        user, is_valid = validate_token(uid, token)
        if not (user and is_valid):
            raise serializers.ValidationError("The token entered is not valid. Please check and try again.")
        self.user = user

        password = data.get('password')
        if password:
            try:
                validate_password(
                    password=password,
                    user=self.user,
                    password_validators=get_password_validators(settings.AUTH_PASSWORD_VALIDATORS)
                )
            except Exception as e:
                raise serializers.ValidationError(e)
        return data


class ResetPasswordConfirmSerializer(TokenValidateMixin, serializers.Serializer):
    password = serializers.CharField()
    token = serializers.CharField()
    uid = serializers.CharField()

    def reset_password(self):
        password = self.validated_data.get('password')
        
        pre_password_reset.send(sender=self.__class__, user=self.user)
    
        self.user.set_password(password)
        self.user.save()

        post_password_reset.send(sender=self.__class__, user=self.user)

        return


class ValidateTokenSerializer(TokenValidateMixin, serializers.Serializer):
    token = serializers.CharField()
    uid = serializers.CharField()


class ActivateInvitedUserSerializer(serializers.Serializer):
    uid = serializers.CharField()
    password = serializers.CharField()

    def validate_uid(self, value):
        try:
            user_id = force_text(urlsafe_base64_decode(value))
            user = User.objects.get(pk=user_id)
            self.user = user
            if self.user.is_active:
                raise serializers.ValidationError(detail='This user has already been activated')

        # use a more specifi error, else everythin above is caught        
        except (TypeError, ValueError, OverflowError, User.DoesNotExist): 
            raise serializers.ValidationError(detail='The uid entered is not valid')

    def validate_password(self, value):
        try:
            validate_password(
                password=value,
                user=self.user,
                password_validators=get_password_validators(settings.AUTH_PASSWORD_VALIDATORS)
            )
            return value
        except Exception as e:
            raise serializers.ValidationError('Please use a more secure password') 

    def activate(self):
        password = self.validated_data['password']
        self.user.is_active=True
        self.user.email_verified=True
        self.user.set_password(password)
        self.user.save()
        return self.user

class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField()
    new_password = serializers.CharField()

    def validate(self, attrs):
        new_password = attrs.get('new_password')
        old_password = attrs.get('old_password')

        if not self.context['request'].user.check_password(old_password):
            raise serializers.ValidationError('Your old password is incorrect')

        try:
            validate_password(
                password=new_password,
                user=self.context['request'].user,
                password_validators=get_password_validators(settings.AUTH_PASSWORD_VALIDATORS)
            )
            return attrs
        except Exception as e:
            raise serializers.ValidationError('Please use a more secure password') 

    def apply_password(self):
        user = self.context['request'].user
        new_password = self.validated_data['new_password']
        user.set_password(new_password)
        user.save()
        # TODO: Mail user about password change
        return user
    

class ContactUsSerializer(serializers.Serializer):
    email = serializers.EmailField()
    message = serializers.CharField()
    full_name = serializers.CharField()
    phone = serializers.CharField()
    business_name = serializers.CharField()
    volume = serializers.CharField()
    country = serializers.ChoiceField(choices=('Nigeria','Ghana','South Africa', 'Other'))
    
    def send(self):
        from django.core.mail import send_mail
        
        data = self.validated_data
        subject = f"Enquiry from: {data.get('business_name')}"
        body = f'''
                Business Name: {data.get('business_name')}
                Full Name: {data.get('full_name')}
                Email Address: {data.get('email')}
                Phone Number: {data.get('phone')}
                Volume: {data.get('volume')}
                Country: {data.get('country')}

                Message: \n {data.get('message')}

            '''
        return send_mail(subject, body, data.get('email'), ['mirapaymentsltd@gmail.com'], fail_silently=False)