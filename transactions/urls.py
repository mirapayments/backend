from django.urls import path

from transactions import views


urlpatterns = [
    path('<str:account_number>/', views.ListTransactionView.as_view()),
    path('create/<str:account_number>/', views.CreatePaymentLinkTransactionView.as_view()),
    path('transaction/<int:pk>/', views.RetrieveTransactionView.as_view()),
    path('export/<str:account_number>/', views.ExportTransactionView.as_view()),
    path('charge/<str:account_number>/', views.CreateCheckoutTransactionView.as_view()),
    path('verify/<str:reference>/', views.VerifyTransactionView.as_view()),
]