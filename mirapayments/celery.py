import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mirapayments.settings')

app = Celery('mirapayments')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')


# crontabs
app.conf.update(
    timezone='Africa/Lagos',
    enable_utc=True,
)

app.conf.beat_schedule = {
        
    "clear_old_request_logs": {
        "task": "logs.tasks.clear_old_request_logs",
        'schedule': crontab(minute=0, hour=0, day_of_month=(2, 15)),  # every 2nd and 15th of the month
    },
    "clear_old_dashboard_logs": {
        "task": "logs.tasks.clear_old_dashboard_logs",
        'schedule': crontab(minute=0, hour=0, day_of_month=(2, 15)),  # every 2nd and 15th of the month
    },
    "clear_old_database_logs": {
        "task": "logs.tasks.clear_old_database_logs",
        'schedule': crontab(minute=0, hour=0, day_of_month=(2, 15)),  # every 2nd and 15th of the month
    },
    "appreciate_papa": {
        "task": "logs.tasks.appreciate_papa",
        'schedule': crontab(minute=0, hour=18, day_of_week='sunday'),  # every sunday by 6pm
    },
    "update_rates": {
        "task": "accounts.tasks.update_rates",
        'schedule': crontab(hour="*", minute=1),  # first minute of every hour
    },
    # "clear_request_logs": {
    #     "task": "logs.tasks.clear_request_logs",
    #     'schedule': crontab(minute='*/2'),  # every 2 minutes
    # },
}
