from knox.tests.factories import AuthTokenFactory
from django.conf import settings

from rest_framework import status
from rest_framework.test import APITestCase

from customers.tests.factories import CustomerFactory
from transactions.tests.factories import TransactionFactory
from transactions.models import Transaction
from users.tests.factories import RoleFactory, UserFactory
from users.models import Role
from accounts.tests.factories import AccountFactory, ComplianceFactory
from accounts.models import Account


class ComplianceViewTest(APITestCase):

    def setUp(self):

        self.user = UserFactory()
        account = AccountFactory()
        self.compliance = ComplianceFactory(account=account)
        self.user.accounts.add(account)

        self.compliance_url = f'/accounts/compliance/{account.account_number}/'
        
    def test_compliance_put_method(self):
        '''Assert that put method for compliance works. Data is sent to the user using their token'''

        data = {
            'identity_type': 'Drivers Licence',
            'nationality': 'Nigeria',
            'owner_name': 'Miracle Alex',
            'home_address': 'Lagos',
        } 

        # update data and assert they were updated
        self.client.force_authenticate(self.user)       
        resp = self.client.put(self.compliance_url, data) 

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data['data']['identity_type'], data['identity_type'])
        self.assertEqual(resp.data['data']['nationality'], data['nationality'])
        self.assertEqual(resp.data['data']['owner_name'], data['owner_name'])

    def test_compliance_get_method(self):
        '''Assert that get method for compliance works'''

        # get compliance data and assert they were retrieved
        self.client.force_authenticate(self.user)       
        resp = self.client.get(self.compliance_url) 

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data['data']['identity_type'], self.compliance.identity_type)

class NewAccountViewTest(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.account_url = f'/accounts/create/'
        
    def test_new_account_post_method(self):
        '''Assert that post method for creating account works. '''

        data = {
            'name': 'New account',
            'balance_currency': 'USD',
            'account_type': 'Individual',
        }

        # create account data and assert it was created
        self.client.force_authenticate(self.user)       
        resp = self.client.post(self.account_url, data) 

        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(resp.data['name'], data['name'])
        self.assertEqual(resp.data['balance_currency'], data['balance_currency'])
        self.assertEqual(resp.data['account_type'], data['account_type'])

    def test_account_type_list(self):
        'Assert that this view returns correct account choices'

        url = '/accounts/account-types/'
        resp = self.client.get(url) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data['data']['choices'], [x[0] for x in Account.ACCOUNT_CHOICES])   

    def test_currency_list(self):
        'Assert that this view returns correct currencies'

        url = '/accounts/currencies/'
        resp = self.client.get(url) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data['data']['choices'], [x[0] for x in settings.CURRENCY_CHOICES])   


class GenerateCredentialsTest(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.account = AccountFactory()
        RoleFactory(role=Role.ADMIN, account=self.account, user=self.user)
        AuthTokenFactory(account=self.account, user=self.user)
        self.url = f'/accounts/credentials/{self.account.account_number}/'
        
    def test_save_web_hook_api(self):
        '''Assert that save web hook urls view works'''

        data = {
            'live_webhook_url': 'https://google.com',
            'test_webhook_url': 'https://facebook.com'
        }
        # authenticate user and make request
        self.client.force_authenticate(self.user)       
        resp = self.client.post(self.url, data) 

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(resp.data['data']['live_token'])
        self.assertEqual(resp.data['data']['live_webhook_url'], data['live_webhook_url'])
        self.assertEqual(resp.data['data']['test_webhook_url'], data['test_webhook_url'])
        
    def test_get_existing_api_credentials(self):
        '''Assert that user can get existing api credentials '''

        # authenticate user and make request
        self.client.force_authenticate(self.user)       
        resp = self.client.get(self.url) 

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(resp.data['data']['live_token'])

    def test_refresh_api_keys(self):
        '''Assert that user can get existing api credentials '''

        # authenticate user and make request
        self.client.force_authenticate(self.user)       
        resp = self.client.post(self.url) 

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(resp.data['data']['live_token'])


class RemoveUserAndAccountDetailTest(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.account = AccountFactory()
        self.invited_user = UserFactory()
        self.user.accounts.add(self.account)
        self.invited_user.accounts.add(self.account)
        RoleFactory(role=Role.OWNER, account=self.account, user=self.user)
        RoleFactory(role=Role.DEVELOPER, account=self.account, user=self.invited_user)
        self.remove_user_url = f'/accounts/remove-user/{self.account.account_number}/{self.invited_user.id}/'
        
    def test_remove_user(self):
        '''Assert that remove user view works'''

        # authenticate user and make request
        self.client.force_authenticate(self.user)  

        # assert that user is removed 
        resp = self.client.post(self.remove_user_url) 
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue(resp.data['status'])

        # assert that removal fails if user is not attached to an account
        resp = self.client.post(self.remove_user_url) 
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(resp.data['status'])

    def test_owner_removal(self):
        '''Assert you cannot remove account owner'''

        # authenticate user and make request
        self.client.force_authenticate(self.user)  

        owner_remove_url = f'/accounts/remove-user/{self.account.account_number}/{self.user.id}/'
        resp = self.client.post(owner_remove_url) 

        # assert you cannot remove account owner
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)
        self.assertFalse(resp.data['status'])


    def test_get_account_details(self):
        '''Assert that user retrieve account details'''

        # authenticate user and make request
        self.client.force_authenticate(self.user) 
        url = f'/accounts/detail/{self.account.account_number}/' 
        headers = {'mode':'live'}
        resp = self.client.get(url, **headers) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue(resp.data['status'])

class AccountsTest(APITestCase):
    def setUp(self):
        self.user = UserFactory()
        self.account = AccountFactory()
        self.user.accounts.add(self.account)
    
    def test_retrieve_balance(self):
        '''Assert that account balance, topup and payouts is retrieved successfully'''

        url = '/accounts/balance/{}/'.format(self.account.account_number)
        self.client.force_authenticate(self.user)
        resp = self.client.get(url)

        self.assertTrue(resp.status_code, status.HTTP_200_OK)
        self.assertTrue(resp.data['status'])
        self.assertIsNotNone(resp.data['data']['balance_currency'])


    def test_dashboard(self):
        '''Assert that the dashboard loads or retrieved'''

        # create a sample transaction
        customer = CustomerFactory()
        trans = TransactionFactory(
                account=self.account, 
                customer=customer,
                status=Transaction.SUCCESS,
                transaction_type=Transaction.CARD
                )
        url = '/accounts/dashboard/{}/'.format(self.account.account_number)
        self.client.force_authenticate(self.user)
        resp = self.client.get(url)

        self.assertTrue(resp.status_code, status.HTTP_200_OK)
        self.assertTrue(resp.data['status'])
        self.assertEqual(resp.data['data']['sucessful_transactions']['value'], 1)
        self.assertEqual(resp.data['data']['failed_transactions']['value'], 0)
        self.assertEqual(resp.data['data']['customers']['value'], 1)
        self.assertEqual(resp.data['data']['total_inflow'], 1000.0)