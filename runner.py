# Use this module to test functions & run arbitrary scripts
#within the context of this application.
# The block below must not be removed and must come first before any imports.
print("Initializing script runner...", end="")
######################################################################
import os                                                            #
import django                                                        #
                                                                     #
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mirapayments.settings")   #
django.setup()                                                       #
######################################################################
print("Ready!")

# from services.visa import get_visa_pull_data, Visa
from transactions.models import Transaction
from accounts.models import Account
from djmoney.money import Money

# tran = Transaction.objects.last()



# data = get_visa_pull_data(tran)

# visa = Visa()
# # resp = visa.pull_funds(data=data)
# resp = visa.validate_card(card_number="4957030420210454", cvv="999", expiry_date="2040-10")
# print(resp)
# print(resp.status_code)
# print(resp.json())

# -->Visa Response
# <Response [200]>
# 200
# {'transactionIdentifier': 135875802776642, 'approvalCode': '12AB54', 'actionCode': '00', 'responseCode': '5', 'cvv2ResultCode': 'M'}

# from logs.tasks import appreciate_papa
# print(appreciate_papa())



acc = Account.objects.get(account_number='5089395079')
trans = Transaction.objects.filter(account=acc).first()
trans = Transaction.objects.last()
print(trans.service_verify_trans().json())

# m = Money(400, 'NGN')
# # u =acc.credit(m)
# # u.save()
# # print(u.balance)
# fm = m - trans.process_fees()
# trans.account.balance += m
# trans.account.save()
# print(trans.process_fees().amount)
# print(fm.amount)
# # trans.account.balance += fm
# print(trans.account.balance.amount)
# # trans.account.credit(fm)
# # print(trans.account.balance)
# # print("Script Completed")
# # print(fm)
# from decimal import Decimal

# d = Decimal(0)

