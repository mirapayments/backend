from django.contrib.auth.tokens import default_token_generator  
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings


def send_verification_email(user):
	context = {
		'name': user.first_name,
		'uid': urlsafe_base64_encode(force_bytes(user.id)),
		'token': default_token_generator.make_token(user),
	}
	email = user.email
	message = render_to_string('users/verification_email.txt', context)
	subject = 'Verify Your Email'
	send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email])

def send_user_activation_mail(user, account_name):
	context = {
		'email': user.email,
		'uid': urlsafe_base64_encode(force_bytes(user.id)),
        'account_name': account_name
	}
	email = user.email
	message = render_to_string('users/activation_email.txt', context)
	subject = 'Activate Your Account'
	send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email])

def invite_existing_user(email, account_name):
	context = {
		'email': email,
        'account_name': account_name
	}
	message = render_to_string('users/invite_existing_user.txt', context)
	subject = f'You have been added to {account_name} account'
	send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email])

def send_password_reset_mail(user):
	context = {
		'name': user.first_name,
		'uid': urlsafe_base64_encode(force_bytes(user.id)),
		'token': default_token_generator.make_token(user),
	}
	email = user.email
	message = render_to_string('users/password_reset.txt', context)
	subject = 'Reset Your Password'
	send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email])

def send_trans_status_mail(trans, user):
	context = {
		'amount': trans.amount,
		'status': trans.status,
		'reference': trans.reference,
		'created_at': trans.created_at
	}
	subject = 'Transaction Status'
	message = render_to_string('transactions/transaction_status.txt', context)

	return send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [user.email])