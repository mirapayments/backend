from django.test import SimpleTestCase
from django.urls import resolve

from accounts import views


class AccountUrlsResolvesToViewTest(SimpleTestCase):

    def test_retrieve_update_compliance_url_resolves_to_compliance_view(self):
        '''assert that retrieve update compliance url resolves to the update compliance view class'''

        found = resolve('/accounts/compliance/123344/')
        # use func.view_class to get the class for the view
        self.assertEqual(found.func.view_class, views.RetrieveUpdateCompliance)

    def test_account_create_url_resolves_to_view(self):
        '''assert that account create url resolves to the right view class'''

        found = resolve('/accounts/create/')
        self.assertEqual(found.func.view_class, views.AccountCreate)

    def test_list_currency_url_resolves_to_view(self):
        '''assert that list currency url resolves to the correct view class'''

        found = resolve('/accounts/currencies/')
        self.assertEqual(found.func.view_class, views.CurrencyList)

    def test_account_type_list_url_resolves_to_view(self):
        '''assert that account type list url resolves to the correct view class'''

        found = resolve('/accounts/account-types/')
        self.assertEqual(found.func.view_class, views.AccountTypeList)

    def test_generate_credentials_resolves_to_view(self):
        '''assert that generate credentials resolves to the correct view class'''

        found = resolve('/accounts/credentials/123344/')
        self.assertEqual(found.func.view_class, views.GenerateCredentials)

    def test_refresh_keys_resolves_to_view(self):
        '''assert that refresh keys resolves to the correct view class'''

        found = resolve('/accounts/refresh-keys/123344/')
        self.assertEqual(found.func.view_class, views.RefreshKeys)

    def test_remove_user_resolves_to_view(self):
        '''assert that remove user resolves to the correct view class'''

        found = resolve('/accounts/remove-user/123344/2/')
        self.assertEqual(found.func.view_class, views.RemoveUser)

    def test_account_details_resolves_to_view(self):
        '''assert that generate credentials resolves to the correct view class'''

        found = resolve('/accounts/detail/123344/')
        self.assertEqual(found.func.view_class, views.AccountDetailView)

    def test_account_balance_resolves_to_view(self):
        '''assert that balance url resolves to the correct view class'''

        found = resolve('/accounts/balance/123344/')
        self.assertEqual(found.func.view_class, views.BalanceView)

    def test_account_dashboard_resolves_to_view(self):
        '''assert that dashboard url resolves to the correct view class'''

        found = resolve('/accounts/dashboard/123344/')
        self.assertEqual(found.func.view_class, views.DashboardView)
