#!/bin/sh


ssh -tt -o StrictHostKeyChecking=no ubuntu@$SERVER_IP << 'ENDSSH'
  
  cd /home/ubuntu/backend
  export $(cat .env | xargs)

  aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin $AWS_ACCT_ID.dkr.ecr.us-east-2.amazonaws.com

  docker-compose pull

  docker-compose -f docker-compose.yml up -d

  docker system prune -a -f   # remove unused images, containers, networks, build cache

  sudo su  # enter sudo mode to clear cache
  echo 3 >'/proc/sys/vm/drop_caches' # clear cache, buffer and swap to get space
  exit  # exit sudo mode
  free -h  # check available space

  exit  # exit shell

ENDSSH