from rest_framework.test import APITestCase

from accounts.tests.factories import  AccountFactory
from customers.serializers import CustomerSerializer

class CustomerSerializerTest(APITestCase):

    def test_customer_serializer(self):
        '''Test Customer serializer :isvalid() and :create() methods'''

        data = {
            'full_name': 'Miracle Alex',
            'email': 'miracle@mirapayments.com',
            'phone': '08022345678',
        }
        # assert :create() method works as expected
        account = AccountFactory()

        trans_serializer = CustomerSerializer(data=data, context={'account': account})

        # assert serilizers checks validity correctly
        self.assertTrue(trans_serializer.is_valid())       


        acc_instance = trans_serializer.save()
        self.assertEqual(acc_instance.full_name, data['full_name'])