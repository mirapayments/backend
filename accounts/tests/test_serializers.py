from django.http import HttpRequest

from rest_framework.test import APITestCase

from accounts.serializers import ComplianceSerializer, NewAccountSerializer
from users.tests.factories import UserFactory
from accounts.tests.factories import  AccountFactory, ComplianceFactory


class ComplianceSerializerTest(APITestCase):

    def test_compliance_serializer(self):
        '''Test compliance serializer :isvalid() and :update() methods'''

        data = {
            'identity_type': 'Drivers Licence',
            'nationality': 'Nigeria',
            'owner_name': 'Miracle Alex',
            'home_address': 'Lagos',
        }
        account = AccountFactory()
        user = UserFactory()
        compliance = ComplianceFactory(account=account)
        user.accounts.add(account)

        compliance_serializer = ComplianceSerializer(data=data, context={'user': user, 'account': account})

        # assert serilizers checks validity correctly
        self.assertTrue(compliance_serializer.is_valid())

        # assert :update() method works as expected
        self.assertNotEqual(compliance.nationality, data['nationality'])

        updated_compliance = compliance_serializer.update(compliance, compliance_serializer.validated_data)
        self.assertEqual(updated_compliance.nationality, data['nationality'])


class NewAccountSerializerTest(APITestCase):

    def test_new_account_serializer(self):
        '''Test new account serializer :isvalid() and :create() methods'''

        data = {
            'name': 'New account',
            'balance_currency': 'USD',
            'account_type': 'Individual',
        }
        request = HttpRequest()
        request.user = UserFactory()

        acc_serializer = NewAccountSerializer(data=data, context={'request': request})

        # assert serilizers checks validity correctly
        self.assertTrue(acc_serializer.is_valid())

        # assert :create() method works as expected
        acc_instance = acc_serializer.save()
        self.assertEqual(acc_instance.name, data['name'])


