"""
Django settings for mirapayments project.

Generated by 'django-admin startproject' using Django 3.2.3.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

from pathlib import Path
import os
from datetime import timedelta

from corsheaders.defaults import default_headers
from dotenv import load_dotenv

load_dotenv()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

ADMINS = [('Miracle', 'collinsalex50@gmail.com')]

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!

SECRET_KEY = os.environ.get('SECRET_KEY', 'django-insecure-4%gi^p0k48tkr()+72)=fdz)870ig^feefgw6=(gpjzk-em')

# SECURITY WARNING: don't run with debug turned on in production!
ENVIRONMENT = os.environ.get('ENVIRONMENT')

DEBUG = ENVIRONMENT == 'local'

ALLOWED_HOSTS = ['api.mirapayments.com', 'localhost', '127.0.0.1', '0.0.0.0']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django.contrib.sites',

    # third party
    'corsheaders',
    'rest_framework',
    'request',
    'drf_yasg',
    'rest_framework_jwt',
    'rest_framework_jwt.blacklist',
    'djmoney.contrib.exchange',

    # local
    'users',
    'knox',
    'accounts',
    'payments',
    'transactions',
    'customers',
    'logs',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',  # cors
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'request.middleware.RequestMiddleware',
    'logs.middleware.LogMiddleware',
]

ROOT_URLCONF = 'mirapayments.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'mirapayments.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('MIRAPAYMENTS_DB_NAME'),
        'USER': os.environ.get('MIRAPAYMENTS_DB_USER'),
        'PASSWORD': os.environ.get('MIRAPAYMENTS_DB_PASSWORD'),
        'HOST': os.environ.get('MIRAPAYMENTS_DB_HOST'),
        'PORT': os.environ.get('MIRAPAYMENTS_DB_PORT'),
    },    
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Africa/Lagos'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_ROOT = BASE_DIR / 'static'

STATIC_URL = '/static/'

# media settings
MEDIA_ROOT = BASE_DIR / 'media'
MEDIA_URL = '/media/'

USE_THOUSAND_SEPARATOR = True

AUTH_USER_MODEL = 'users.User'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        },
    },
    'handlers': {
        'db_log': {
            'level': 'DEBUG',
            'class': 'logs.db_log_handler.DatabaseLogHandler'
        },
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        }
    },
    'loggers': {
        'db': {
            'handlers': ['db_log'],
            'level': 'DEBUG'
        },
        # suspend disallowed host email
        'django.security.DisallowedHost': {
            'handlers': ['null'],
            'propagate': False,
        },
    }
}

# Rest framework settings powering API
####### DO NOT MODIFY!!!!!  #########
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        # 'rest_framework.authentication.BasicAuthentication',
        'knox.auth.TokenAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 100,
    'DEFAULT_RENDERER_CLASSES': [
        'helpers.api_response.CustomJSONRenderer',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ],
    'DEFAULT_THROTTLE_CLASSES': [
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle'
    ],
    'DEFAULT_THROTTLE_RATES': {
        'anon': '1000/day',
        'user': '5000/day'
    },

}

# Request Log settings
REQUEST_IGNORE_IP = ['127.0.0.1', ]
REQUEST_IGNORE_PATHS = (
    r'^tower/', r'^static/', r'^favicon.ico'
)


# Celery settings
CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL', 'amqp://localhost') 


# Email settings
if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
else:
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_PORT = 587
    EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
    EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
    EMAIL_USE_TLS = True
    DEFAULT_FROM_EMAIL = 'MiraPayments LTD <mirapaymentsltd@gmail.com>'

# django curs (DO NOT MODIFY!!!)
CORS_ALLOW_ALL_ORIGINS = True 

CORS_ALLOW_HEADERS = list(default_headers) + [
    "mode",
]
# django money (DO NOT MODIFY!!!)
CURRENCIES = ('NGN', 'USD', 'EUR', 'GBP')
CURRENCY_CHOICES = [('NGN', 'NGN'), ('USD', 'USD'), ('EUR', 'EUR'), ('GBP', 'GBP')]
# BASE_CURRENCY = 'NGN'

# JWT settings (DO NOT MODIFY!!!)
JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': timedelta(days=2),
    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=2),
    'JWT_AUTH_HEADER_PREFIX': 'Token',
    'JWT_RESPONSE_PAYLOAD_HANDLER':
        'rest_framework_jwt.utils.jwt_create_response_payload',
}

# Default primary key field type
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


# internal documentation settings
SWAGGER_SETTINGS = {
   'SECURITY_DEFINITIONS': {
      'Token': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
      }
   },
   'USE_SESSION_AUTH': False
}

TEXTMEBOT_APIKEY = os.environ.get('TEXTMEBOT_APIKEY')

OPEN_EXCHANGE_RATES_APP_ID = os.environ.get('OPEN_EXCHANGE_RATES_APP_ID')