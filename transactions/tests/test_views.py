import datetime as dt

from rest_framework.test import APITestCase
from rest_framework import status

from users.tests.factories import UserFactory
from accounts.tests.factories import AccountFactory
from transactions.tests.factories import TransactionFactory


class TransactionTest(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.account = AccountFactory()
        self.user.accounts.add(self.account)
        self.trans = TransactionFactory(account=self.account)

    def test_list_transactions(self):
        'Assert that listing transactions for an account works'

        url = f'/transactions/{self.account.account_number}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.get(url) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_retrieve_transaction(self):
        '''Assert that a transaction can be retrieved'''

        url = f'/transactions/transaction/{self.trans.id}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(self.trans.description, resp.data['description'])

    def test_paymentlink_transaction_create(self):
        '''Assert paymentlink transactions are created'''

        today = dt.date.today()
        data = {
            'amount': 100.0,
            'amount_currency': 'USD',
            'description': 'to collect plenty money',
            'source_identifier': 'Single',
            'transaction_type': 'Card',
            'service': 'Visa',
            "customer": {
                "full_name": "Miracle Alex",
                "email": "user@example.com"
            },
            "card": {
                "card_number": "4000040000400004",
                "card_type": "Visa",
                "exp_month": today.month + 1,
                "exp_year": today.year,
                "cvv": "678",
                "country": "NGA"
            },
        }
        url = '/transactions/create/{}/'.format(self.account.account_number)
        resp = self.client.post(url, data, format='json')

         # TODO: mock visa api call for tests
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(data["amount_currency"], resp.data['data']['amount_currency'])

    def test_export_transactions(self):
        '''Assert transactions are exported'''

        url = '/transactions/export/{}/'.format(self.account.account_number)
        self.client.force_authenticate(self.user)   
        resp = self.client.post(url)
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp._content_type_for_repr, ', "text/csv"')