from django.test import SimpleTestCase
from django.urls import resolve

from customers import views


class CustomerUrlsResolvesToViewTest(SimpleTestCase):

    def test_url_resolves_to_list_create_customer_view(self):
        '''assert that list create customer url resolves to the right view class'''

        found = resolve('/customers/123344/')
        # use func.view_class to get the class for the view
        self.assertEqual(found.func.view_class, views.ListCreateCustomerView)

    def test_url_resolves_to_retrieve_update_customer_view(self):
        '''assert that retrieve create customer url resolves to the right view class'''

        found = resolve('/customers/customer/2/')
        self.assertEqual(found.func.view_class, views.RetrieveUpdateCustomerView)