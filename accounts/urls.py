from django.urls import path

from accounts import views


urlpatterns = [
    path('compliance/<str:account_number>/', views.RetrieveUpdateCompliance.as_view()),
    path('create/', views.AccountCreate.as_view()),
    path('account-types/', views.AccountTypeList.as_view()),
    path('currencies/', views.CurrencyList.as_view()),
    path('credentials/<str:account_number>/', views.GenerateCredentials.as_view()),
    path('refresh-keys/<str:account_number>/', views.RefreshKeys.as_view()),
    path('detail/<str:account_number>/', views.AccountDetailView.as_view()),
    path('remove-user/<str:account_number>/<int:user_id>/', views.RemoveUser.as_view()),
    path('balance/<str:account_number>/', views.BalanceView.as_view()),
    path('dashboard/<str:account_number>/', views.DashboardView.as_view()),
]