from django.test import TestCase

from accounts.tests.factories import AccountFactory, ComplianceFactory


class AccountTest(TestCase):

    def test_model_creation(self):
        '''Assert that the Account model was created with correct defaults'''

        account = AccountFactory()

        self.assertIsNotNone(account.public_key) 
        self.assertIsNotNone(account.account_type) 
        self.assertIsNotNone(account.account_number) 
        self.assertTrue(account.is_active) 
        self.assertFalse(account.setup_complete) 
        self.assertIsNotNone(account.name) 

class ComplianceTest(TestCase):

    def test_model_creation(self):
        '''Assert that the Compliance model was created with correct defaults'''

        compliancce = ComplianceFactory()

        self.assertIsNotNone(compliancce.identity_type) 
        self.assertEqual(compliancce.contact_email, '') 
        self.assertEqual(compliancce.owner_name, '') 
        self.assertEqual(compliancce.nationality, '') 
        self.assertEqual(compliancce.contact_number, '') 
