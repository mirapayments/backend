#!/bin/bash

set -x
set -eu
set -o pipefail

python3 -m venv env

source env/bin/activate

pip3 install -r requirements.txt

export ENVIRONMENT="build"
export MIRAPAYMENTS_DB_NAME="test"
export MIRAPAYMENTS_DB_USER="test"
export MIRAPAYMENTS_DB_PASSWORD="password"
export MIRAPAYMENTS_DB_HOST="localhost"
export MIRAPAYMENTS_DB_PORT=5432


python manage.py check

python manage.py test