from django.contrib import admin

from payments.models import PaymentLink, Checkout


@admin.register(PaymentLink)
class PaymentLinkAdmin(admin.ModelAdmin):
    list_display = ['name', 'payment_type', 'account', 'amount_currency']
    list_filter = ['payment_type', 'is_active', 'interval']
    search_fields = ['code']
    raw_id_fields = ('account',)
    date_hierarchy = 'created_at'


@admin.register(Checkout)
class CheckoutAdmin(admin.ModelAdmin):
    list_display = ['amount','amount_currency', 'code', 'redirect_url', ]
    search_fields = ['code']
