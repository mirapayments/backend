from django.urls import path

from rest_framework_jwt.views import refresh_jwt_token, obtain_jwt_token, verify_jwt_token

from users import views



urlpatterns = [
    path('token/', obtain_jwt_token, name='obtain_token'),
    path('token/refresh/', refresh_jwt_token, name='token_refresh'),
    path('token/verify/', verify_jwt_token, name="token_verify"),
    path('login/', views.LoginView.as_view()),
    path('signup/', views.SignUpView.as_view()),
    path('me/', views.UserDetailUpdateView.as_view()),
    path('logout/', views.LogoutView.as_view()),
    path('verify-email/<str:uidb64>/<str:token>/', views.VerifyEmail.as_view()),
    path('send-verification-email/', views.SendVerificationEmail.as_view()),
    path('password-reset-request/', views.ResetPasswordRequest.as_view()),
    path('reset-password-validate-token/', views.ResetPasswordValidateToken.as_view()),
    path('reset-password-confirm/', views.ResetPasswordConfirm.as_view()),
    path('invite/<str:account_number>/', views.UserInvitationView.as_view()),
    path('list/<str:account_number>/', views.ListAccountUsersView.as_view()),
    path('me/accounts/', views.ListUserAccountsView.as_view()),
    path('me/change-password/', views.ChangePassword.as_view()),
    path('activate/', views.ActivateInvitedUser.as_view()),
    path('cicd-verify-email/<str:email>/', views.CICDVerifyEmail.as_view()),
    path('contact-us/', views.ContactUsView.as_view()),
]
