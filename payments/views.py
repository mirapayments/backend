from django.shortcuts import get_object_or_404

from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema

from knox import defaults
from helpers.api_response import SuccessResponse
from accounts.models import Account
from accounts.permissions import IsAccountMember
from payments.serializers import CheckoutSerializer, PaymentLinkSerializer
from payments.models import Checkout, PaymentLink
from transactions.models import Transaction
from transactions.serializers import TransactionSerializer


class CreateListPaymentLinkView(generics.ListCreateAPIView):
    '''
    Create and List payment links for an account
    POST or GET /payments/payment-link/<str:account_number>/
    '''    
    queryset = PaymentLink.objects.all().order_by('-created_at')
    serializer_class = PaymentLinkSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = None

    def perform_create(self, serializer):
        account_number = self.kwargs.get('account_number')
        account = get_object_or_404(Account, account_number=account_number)
        serializer.save(account=account)

    def get_queryset(self):
        account_number = self.kwargs.get('account_number')
        queryset = super().get_queryset().filter(account__account_number=account_number)
        return queryset


class RetrieveUpdateDeletePaymentLinkView(generics.RetrieveUpdateDestroyAPIView):
    '''
    Retrieve or Delete or Update payment link for an account
    Get or PUT or DELETE /payments/payment-link/<int:pk>/<str:account_number>/
    pk - user id
    account_number - the account's number
    '''    
    serializer_class = PaymentLinkSerializer
    queryset = PaymentLink.objects.all()
    permission_classes = [IsAuthenticated]
    pagination_class = None
    lookup_field = 'pk'    

    def get_queryset(self):
        # only perfom action to payment links attached to account
        account_number = self.kwargs.get('account_number')
        queryset = super().get_queryset().filter(account__account_number=account_number)
        return queryset
    
    def get(self, request, *args, **kwargs):
        paymentlink = self.get_object()
        trans = Transaction.objects.filter(source_reference=paymentlink.code)
        response = super().get(request, *args, **kwargs)
        response.data['transactions'] = TransactionSerializer(trans, many=True).data
        return response

class TogglePaymentLink(APIView):
    '''
    Deactivate or actvate a payment link
    POST - /payments/payment-link/toggle/<int:pk>/<str:account_number>/
    pk - user id
    account_number - the account's number
    '''
    permission_classes = [IsAuthenticated, IsAccountMember]

    @swagger_auto_schema( responses={200: PaymentLinkSerializer})
    def post(self, request, pk, account_number):
        payment_link = get_object_or_404(PaymentLink, pk=pk)
        payment_link.is_active = not payment_link.is_active
        payment_link.save()
        data = PaymentLinkSerializer(payment_link).data
        return SuccessResponse(data=data)

class CodeRetrievePaymentLink(APIView):
    '''
    Retrieve a payment link using it's unique code
    GET /payments/payment-link/code/<code>/
    code - unique code
    '''
    permission_classes = [AllowAny]

    @swagger_auto_schema( responses={200: PaymentLinkSerializer})
    def get(self, request, code):
        payment_link = get_object_or_404(PaymentLink, code=code)
        data = PaymentLinkSerializer(payment_link).data
        data['account_detail'] = {
            'account_name': payment_link.account.name,
            'public_key': '{}{}'.format(defaults.LIVE_PUBLIC_KEY_PREFIX, payment_link.account.live_pk),
        }
        return SuccessResponse(data=data)


class CreateCheckout(APIView):
    '''
    create a checkout link [pub]
    POST /payments/checkout/create/
    '''
    permission_classes = [IsAuthenticated]

    #TODO: add tests
    @swagger_auto_schema(request_body=CheckoutSerializer,
                        operation_summary='create a checkout link',
                        responses={200: CheckoutSerializer})
    def post(self, request):
        serializer = CheckoutSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return SuccessResponse(data=serializer.data, )


class RetrieveCheckout(APIView):
    '''
    Retrieve checkout [pub]
    GET /payments/checkout/code/:code/
    '''
    permission_classes = [AllowAny]

    #TODO: add tests
    @swagger_auto_schema(operation_summary='retrieve a checkout link',
                        responses={200: CheckoutSerializer})
    def get(self, request, code):
        checkout = get_object_or_404(Checkout, code=code)
        serializer = CheckoutSerializer(checkout)
        
        return SuccessResponse(data=serializer.data, )