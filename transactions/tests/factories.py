import factory
from djmoney.money import Money

from transactions.models import Transaction


class TransactionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'transactions.Transaction'

    transaction_type = factory.Iterator([x[0] for x in Transaction.TRANSACTION_TYPES])
    amount =  Money(1000, 'NGN')
    fee =  Money(100, 'NGN')
    amount_due =  Money(1000, 'NGN')
    balance_after_transaction = Money(2000, 'NGN')
    description = factory.Sequence(lambda n: 'transaction description {}'.format(n))
    account = factory.SubFactory('accounts.tests.factories.AccountFactory')
    service = factory.Iterator([x[0] for x in Transaction.SERVICE_CHOICES])
    source_identifier = factory.Iterator([x[0] for x in Transaction.SOURCE_IDENTIFIER_TYPES])
    status = factory.Iterator([x[0] for x in Transaction.TRANSACTION_STATUS])
    customer = factory.SubFactory('customers.tests.factories.CustomerFactory')
    transaction_type = factory.Iterator([x[0] for x in Transaction.TRANSACTION_TYPES])
