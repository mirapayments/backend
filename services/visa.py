import json
import os
import datetime
# import logging
# import http.client as http_client

import requests
from dotenv import load_dotenv

from helpers.utils import generate_random_number
from helpers import constants

# http_client.HTTPSConnection.debuglevel = 0
load_dotenv()

# # You must initialize logging, otherwise you'll not see debug output.
# logging.basicConfig()
# logging.getLogger().setLevel(logging.DEBUG)
# requests_log = logging.getLogger("requests.packages.urllib3")
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True

now = datetime.datetime.now()
now_str = now.strftime("%Y-%m-%dT%H:%M:%S")

##############################################
##############################################
# PROTECTED CLASS, DO NOT MODIFY!!!
##############################################
##############################################

class Visa:
   
    def __init__(self, base_url=None, cert=None, key=None, password=None, user_id=None, timeout=35):
        self.user_id = user_id or os.environ.get('VISA_USERID')
        self.password = password or os.environ.get('VISA_PASSWORD')
        self.cert = cert or 'services/certificates/visa_cert.pem'
        self.key = key or 'services/certificates/visa_private_key.pem'
        self.timeout = timeout
        self.base_url = base_url or 'https://sandbox.api.visa.com'
    
    def request(self, method: str, url: str, headers: dict = {}, params: dict = {}, data: dict = {}):
        '''
        Send requests to Visa using Two-Way (Mutual) SSL
        Visa Requests will timeout after 30 secs (visa default)
        Python request will timeout after timeout value set in __init__
        '''

        # Only needed when trying to simulate timeout, Visa already set 30 secs as default
        # headers['X-Transaction-Timeout-MS'] = str(self.timeout) 
        headers['Accept'] = 'application/json'
        response = requests.request(
                                method,
                                url,
                                cert=(self.cert, self.key),
                                headers=headers,
                                auth=(self.user_id, self.password),
                                json=data,
                                timeout=self.timeout,
                                params=params
                            )
        return response

    def hello_world(self):
        '''Mutual auth hello world request'''

        url = self.base_url + '/vdp/helloworld'
        response = self.request(
                            method='get', 
                            url=url,
                        )
        return response


    def pull_funds(self, data):
        '''
        pull (debit) funds from the sender's Visa account
        https://developer.visa.com/capabilities/visa_direct/reference#visa_direct__funds_transfer__v1__pullfunds
        '''
        url = self.base_url + '/visadirect/fundstransfer/v1/pullfundstransactions'
        # data = data.update(data)
        response = self.request(
                            method='post',
                            url=url,
                            data=data,
                        )
        return response

    def push_funds(self, data):            
        '''
        push funds are to the sender's Visa account
        https://developer.visa.com/capabilities/visa_direct/reference#visa_direct__funds_transfer__v1__pushfunds
        '''
        url = self.base_url + '/visadirect/fundstransfer/v1/pushfundstransactions'
        # data = data.update(data)
        response = self.request(
                            method='post',
                            url=url,
                            data=data,
                        )
        return response

    def reverse_funds(self, data):
        pass    

    def query_api(self, bin=None, **kwargs):
        '''
        Used to query the status and get details of a timeout transaction or 
        get details of a successful/failed transaction at a later date

        kwargs:
            -transactionIdentifier
            - san
            - rrn

        If `transactionIdentifier` is unavilable, then provide a `san` and `rrn`    
        '''

        url = self.base_url + '/visadirect/v1/transactionquery'
        params = {
            'acquiringBIN': bin or constants.VISA_BIN,
        }
        for k, v in kwargs.items():
            params[k] = v

        response = self.request(
                            method='get',
                            url=url,
                            params=params
        )
        return response
    
    def validate_card(self, card_number, cvv, expiry_date):
        '''
        Used to validate a card
        https://developer.visa.com/capabilities/pav/reference#tag/Payment-Account-Validation-API/

        kwargs:
            - card_number, e.g: 4957030420210462
            - cvv, e.g: 022
            - expiry_date, e.g: 2040-10
        '''
        
        url = self.base_url + '/pav/v1/cardvalidation'
        data = {
            "acquiringBin": constants.VISA_BIN, #req
            "acquirerCountryCode": constants.VISA_COUNTRY_CODE, #req, Nigeria, 566
            "primaryAccountNumber": card_number, #"4957030420210462", #req
            "cardCvv2Value": cvv, # 022, req or use AVS(address)
            "cardExpiryDate": expiry_date
        }

        response = self.request(
                            method='post',
                            url=url,
                            data=data
        )
        return response





    
# push_data = {
#   "amount": "124.05",  #req
#   "senderAddress": "901 Metro Center Blvd",
#   "localTransactionDateTime": now,  #req
#   "recipientPrimaryAccountNumber": "4957030420210496",  #req
#   "cardAcceptor": {  #req
#     "address": {  #req
#       "country": "NGA",  #req
#     },
#     "idCode": "CA-IDCode-77765",  #req
#     "name": "Visa Inc. USA-Foster City",  #req
#     "terminalId": "TID-9999"  #req
#   },
#   "senderReference": "",
#   "transactionIdentifier": "381228649430015", #no need for this if it's one way
#   "acquirerCountryCode": "840", #req
#   "acquiringBin": "408999", #req
#   "retrievalReferenceNumber": "412770451018",  #req
#   "systemsTraceAuditNumber": "451018",  #req
#   "senderName": "Mohammed Qasim",
#   "businessApplicationId": "AA",  #req
#   "transactionCurrencyCode": "USD",  #req
#   "senderAccountNumber": "4653459515756154"
# }

# pull_data = {
#   "surcharge": "11.99",
#   "amount": "124.02", # req
#   "localTransactionDateTime": now, # req
#   "cpsAuthorizationCharacteristicsIndicator": "Y",
# #   "riskAssessmentData": {
# #     "traExemptionIndicator": True,
# #     "trustedMerchantExemptionIndicator": True,
# #     "scpExemptionIndicator": True,
# #     "delegatedAuthenticationIndicator": True,
# #     "lowValueExemptionIndicator": True
# #   },
#   "cardAcceptor": { # req
#     "address": { # req
#       "country": "USA", # req
#       "zipCode": "94404",
#       "county": "081",
#       "state": "CA"
#     },
#     "idCode": "ABCD1234ABCD123", # req
#     "name": "Visa Inc. USA-Foster City", # req
#     "terminalId": "ABCD1234" # req
#   },
#   "acquirerCountryCode": "840", # req
#   "acquiringBin": "408999",  # req
#   "senderCurrencyCode": "NGN",
#   "retrievalReferenceNumber": "330000550000", # req
# #   "addressVerificationData": {
# #     "street": "XYZ St",
# #     "postalCode": "12345"
# #   },
# #   "cavv": "0700100038238906000013405823891061668252",
#   "systemsTraceAuditNumber": "451001", 
#   "businessApplicationId": "AA", # req
#   "senderPrimaryAccountNumber": "4895142232120006",
# #   "settlementServiceIndicator": "9",
#   "visaMerchantIdentifier": "73625198",
# #   "foreignExchangeFeeTransaction": "11.99",
#   "senderCardExpiryDate": "2015-10",
# #   "nationalReimbursementFee": "11.22"
# }

def generate_visa_reference(now):

    year_last_digit = now.strftime("%Y")[-1]
    day_of_year = now.strftime("%j")
    hour = now.strftime("%H")
    code = generate_random_number(6)
    return "{}{}{}{}".format(year_last_digit, day_of_year, hour, code)

def get_visa_pull_data(trans):
    card = trans.card
    return {
        "surcharge": "11.99",
        "amount": float(trans.amount.amount), # req
        "localTransactionDateTime": now_str, # req
        "cpsAuthorizationCharacteristicsIndicator": "Y",
        "cardAcceptor": { # req
            "address": { # req
                "country": card.country, # req
            },
            "idCode": "5656AB", # req
            "name": "Mirapayments LTD", # req, e.g Visa Inc. USA-Foster City
            "terminalId": "12AD" # req
        },
        "acquirerCountryCode": constants.VISA_COUNTRY_CODE, # req, Nigeria
        "acquiringBin": constants.VISA_BIN,  # req
        "senderCurrencyCode": trans.amount_currency, #req, e.g NGN, USD
        "retrievalReferenceNumber": generate_visa_reference(now), # req
        "systemsTraceAuditNumber": generate_random_number(6), 
        "businessApplicationId": "PP", # req
        "senderPrimaryAccountNumber": card.card_number, # "4895142232120006"
        #   "settlementServiceIndicator": "9",
        "visaMerchantIdentifier": "73625198",
        #   "foreignExchangeFeeTransaction": "11.99",
        "senderCardExpiryDate": "{}-{}".format(card.exp_year, card.exp_month), # "2015-10" req
        #   "nationalReimbursementFee": "11.22",
          "merchantCategoryCode": "566"

    }

# visa = Visa(timeout=3)
# visa = Visa()
# resp = visa.push_funds(data=push_data)
# resp = visa.query_api('408999', '234234322342343')
# print(resp.text)
# print(resp.headers)
# correlation_id = resp.headers.get('X-CORRELATION-ID')
# trans_identifier = resp.json().get('transactionIdentifier')
# print(resp.json())

# print(data)

# resp = visa.hello_world()

# print(resp.json())

# resp = visa.pull_funds(data=pull_data)
# print(resp)

# implement
# handle timeout transactions
# trans status/ verification
# reverse trans
# push trans

