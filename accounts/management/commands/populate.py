from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command


class Command(BaseCommand):
    help = "populate database tables with initial data"

    def handle(self, *args, **options):
        
        if settings.ENVIRONMENT == "local":
         
            self.stdout.write(self.style.MIGRATE_HEADING("Populating the database, please wait..."))
            # populate complaince
            call_command('loaddata', 'accounts/fixtures/compliance.json', '-i')
        
            # populate accounts
            call_command('loaddata', 'accounts/fixtures/account.json', '-i')
        
            # populate transactions
            call_command('loaddata', 'transactions/fixtures/transaction.json', '-i')
        
            # populate customers
            call_command('loaddata', 'customers/fixtures/customer.json', '-i')
        
            # populate payment links
            call_command('loaddata', 'payments/fixtures/paymentlink.json', '-i')
        
            # populate users
            call_command('loaddata', 'users/fixtures/user.json', '-i')
        
            # populate roles
            call_command('loaddata', 'users/fixtures/role.json', '-i')
        
            self.stdout.write(self.style.SUCCESS("All Done!!!"))

        else:
            self.stdout.write(self.style.ERROR("This command is only permitted for development environments"))
      