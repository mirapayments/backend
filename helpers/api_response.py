from collections import OrderedDict

from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer



#####################################
#####################################
# DO NOT MODIFY THIS FILE
######################################
######################################


class SuccessResponse:
    '''Standardise API Responses and add additional keys'''
    
    def __new__(cls,  data={}, detail = 'Success', status=status.HTTP_200_OK):
        return Response(
            {
                'status': True,
                'detail': detail,
                'data': data
            },
            status
        )

class FailureResponse:
    def __new__(cls, detail = 'Error', status=status.HTTP_400_BAD_REQUEST):
        return Response(
            {
                'status': False,
                'detail': detail
            },
            status
        )


class CustomJSONRenderer(JSONRenderer):
    '''Override the default JSON renderer to be consistent and have additional keys'''

    def render(self, data, accepted_media_type=None, renderer_context=None):
        status = renderer_context['response'].status_code < 400
        
        # when response data is a list, convert to dict
        if isinstance(data, list):
            data = {
                'status': status,
                'detail': 'Success' if status else 'Error',
                'data': data
            }
    
        # if response data is none, convert to dict
        if data is None:
            data = {}

        # if response data is not well formated dict, and not an error response, convert to right format
        if isinstance(data, OrderedDict) and ('data' not in data) and ('non_field_errors' not in data):
            data = {
                'status': status,
                'detail': 'Success' if status else 'Error',
                'data': data,
            }

        # convert non_field_errors message to text, instead of list
        if 'non_field_errors' in data:
            data['detail'] = data.pop('non_field_errors')[0]
        
        # if not `status` in response, add status
        if not 'status' in data:
            data['status'] = status
   
        # if no `detail` in response, add detail based on status
        if not 'detail' in data:
            data['detail'] = 'Success' if status else 'Error'

        # Added this so we can use `.move_to_end` to make `status` & `detail` come on top  
        # if isinstance(data, dict):
            # data = OrderedDict(data.items()) 

        # removing to increase spead, since this is O(N), and only needed when 
        # returning some error responses 
        # data.move_to_end('detail', last=False)
        # data.move_to_end('status', last=False)

        return super().render(data, accepted_media_type, renderer_context)




# from django.db import transaction
# from django.db.utils import IntegrityError, ProgrammingError
# from django.http import Http404

# from drf_yasg import openapi
# from rest_framework import status
# from rest_framework.exceptions import ValidationError, NotAuthenticated, ParseError, AuthenticationFailed
# from rest_framework.renderers import JSONRenderer
# from rest_framework.response import Response
# from rest_framework.views import exception_handler

# def check_exceptions(self, func, request, *args, **kwargs):
# 	try:
# 		with transaction.atomic():
# 			return func(self, request, *args, **kwargs)
# 	except ProgrammingError as p:
# 		return APIFailure(message=p.__str__().split('\n')[0], status=status.HTTP_404_NOT_FOUND)
# 	except ValidationError as e:
# 		codes = e.get_codes()
# 		field = list(e.__dict__['detail'].keys())[0]
# 		error_is_list = isinstance((e.__dict__['detail'][field]), list)
# 		if error_is_list:
# 			error = str(e.__dict__['detail'][field][0])
# 		else:
# 			dict__ = e.__dict__['detail'][field]
# 			keys = list(dict__.keys())[0]
# 			# detail = dict__[keys]
# 			error = f"{field} has an invalid input"

# 		if isinstance(codes[field], list):
# 			codes = [x for x in codes[field] if x != {}]
# 			code = codes[0] if len(codes) > 0 else None
# 			if isinstance(code, list):
# 				data = e.__dict__['detail'][field]
# 				id_s = [e.__dict__['detail'][field][_][0].split('"')[1] for _, i in enumerate(data) if i != {}]
# 				field = field
# 				if code == ['does_not_exist']:
# 					error = f"No {field} with the following IDs ({', '.join(id_s)}) exists."
# 			if isinstance(code, dict) and code != {}:
# 				error = f'{list(code.keys())[0]} {code[list(code.keys())[0]][0]}'
# 		else:
# 			code = None
# 		if 'this field' in error:
# 			error = error.replace("this field", f"that the '{field}' field")
# 		elif code == 'required':
# 			error = error.replace('This field', f"The '{field}' field")
# 		elif code == 'invalid_choice':
# 			tag = error.split('"')[1]
# 			rest = error.split('"')[2][:-1]
# 			error = f"'{tag}'{rest} in the '{field}' field."
# 		elif code == 'incorrect_type':
# 			error = error.replace('Incorrect', f"The '{field}' field has an incorrect").replace('pk', 'int')
# 		elif code == 'does_not_exist':
# 			pk = error.split('"')[1]
# 			print(pk, '       pkkkkkkkkk')
# 			error = f"The {field} with ID '{pk}' does not exist."
# 		elif code == 'invalid':
# 			error = error.replace('Invalid', f"The '{field}' field has an invalid"). \
# 				replace('pk', 'int').replace('a dictionary', 'an object')
# 		elif code == 'null':
# 			error = f"The field '{field}' cannot be empty"
# 		return APIFailure(message=error, status=status.HTTP_400_BAD_REQUEST)
# 	except ParseError:
# 		return APIFailure(message="Please check your request data.",
# 						  status=status.HTTP_400_BAD_REQUEST)
# 	except NotAuthenticated as e:
# 		return APIFailure(message=e.detail.__str__(), status=e.status_code)
# 	except AuthenticationFailed as e:
# 		return APIFailure(message=e.detail.__str__(), status=e.status_code)
# 	except IntegrityError as e:
# 		return APIFailure('Duplicate Transaction')
# 	except Exception as e:

# 		print(e.__str__(), type(e))
# 		error_msg = e.__str__().split('\n')[0].replace('"', "'")
# 		return APIFailure(message=error_msg,
# 						  status=status.HTTP_404_NOT_FOUND if type(e) == Http404 else status.HTTP_400_BAD_REQUEST)



# def api_exception(func):
# 	def inner(self, request, *args, **kwargs):

# 		return check_exceptions(self, func, request, *args, **kwargs)
 
# 	return inner


# def api_exception(func):
#     def inner(self, request, *args, **kwargs):
#         try:
#             return check_exceptions(self, func, request, *args, **kwargs)
#         except Exception as e:
#             print(e, type(e), e.__str__())
#             error_msg = e.__str__().split('\n')[0].replace('"', "'")
#             return FailureResponse(detail=error_msg,
# 							  status=status.HTTP_404_NOT_FOUND if type(e) == Http404 else status.HTTP_400_BAD_REQUEST)
#         return inner