from django.test import TestCase

from payments.tests.factories import PaymentLinkFactory


class PaymentLinkTest(TestCase):

    def test_model_creation(self):
        '''Assert that the PaymentLink model was created with correct defaults'''

        payment_link = PaymentLinkFactory()

        self.assertIsNotNone(payment_link.account) 
        self.assertIsNotNone(payment_link.payment_type) 
        self.assertIsNotNone(payment_link.code) 
        self.assertTrue(payment_link.is_active) 
        self.assertIsNotNone(payment_link.name) 