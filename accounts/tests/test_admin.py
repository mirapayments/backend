from django.test import TestCase
from django.urls import reverse
from django.contrib.admin.sites import AdminSite

from users.tests.factories import UserFactory
from accounts import admin
from accounts.tests.factories import AccountFactory, ComplianceFactory
from accounts.models import Account


class AccountAdminTest(TestCase):

    def setUp(self):
        self.user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login(self.user)
        self.acct = AccountFactory()

        # instantiate adminsite
        site = AdminSite() 
        self.acct_admin = admin.AccountAdmin(Account, site)

    def test_changelist_view(self):
        '''Assert account change list view loads well'''

        url = reverse("admin:%s_%s_changelist" % (self.acct._meta.app_label, self.acct._meta.model_name))
        page = self.client.get(url)
        self.assertEqual(page.status_code, 200)

    def test_change_view(self):
        '''Assert account change view page opens successfully'''

        url = reverse("admin:%s_%s_change" % (self.acct._meta.app_label, self.acct._meta.model_name), args=(self.acct.pk,))
        page = self.client.get(url)
        self.assertEqual(page.status_code, 200)


    def test_export_as_csv(self):
        '''Assert that export_to_csv() works'''

        queryset = AccountFactory.create_batch(size=10)
        
        result = self.acct_admin.export_items_to_csv(self.client.request, queryset)
        #csv is created and returns success status code
        self.assertEqual(result.status_code, 200) 


class ComplianceAdminTest(TestCase):

    def setUp(self):
        self.user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login(self.user)
        self.compliance = ComplianceFactory()

    def test_changelist_view(self):
        '''Assert account change list view loads well'''

        url = reverse("admin:%s_%s_changelist" % (self.compliance._meta.app_label, self.compliance._meta.model_name))
        page = self.client.get(url)
        self.assertEqual(page.status_code, 200)

    def test_change_view(self):
        '''Assert account change view page opens successfully'''

        url = reverse("admin:%s_%s_change" % (self.compliance._meta.app_label, self.compliance._meta.model_name), args=(self.compliance.pk,))
        page = self.client.get(url)
        self.assertEqual(page.status_code, 200)