from django.db import models

from djmoney.models.fields import MoneyField
from djmoney.models.validators import MinMoneyValidator

from helpers.utils import generate_unique_key

class PaymentLink(models.Model):
    '''
    The model to store Payment Links

    - name: the payment link name
    - code: the unique code for payment links
    - amount: the amount of the payment link, managed with django-money
    - account: the account which the payment link is tied to
    - fixed_amount: whether amount is fixed or not
    - is_active: whether payment is suspended or active
    - redirect_url: the url to redirect after payment
    - description: the payment link description
    - extra_info: extra info on the payment
    - notify: the email to notify after transaction 
    - interval: the payment intervals in the case of subscriptions
    - background_image: the background image 
    - donation_website: the website for donation
    - donation_phone: the phone number for donation
    - payment_type: payment types
    - created_at: time account was created
    - updated_at: time account was updated
    '''
    SINGLE = 'Single'
    SUBSCRIPTION = 'Subscription'
    PRODUCT = 'Product'
    DONATION = 'Donation'
    PAYMENT_CHOICES = (
        (SINGLE, SINGLE),
        (SUBSCRIPTION, SUBSCRIPTION),
        (PRODUCT, PRODUCT),
        (DONATION, DONATION)
    )
    HOURLY = 'Hourly'
    DAILY = 'Daily'
    WEEKLY = 'Weekly'
    MONTHLY = 'Monthly'
    QUARTERLY = 'Quarterly'
    HALF_YEARLY = 'Half-Yearly'
    YEARLY = 'Yearly'
    INTERVAL_CHOICES = (
        (HOURLY, HOURLY),
        (DAILY, DAILY),
        (WEEKLY, WEEKLY),
        (MONTHLY, MONTHLY),
        (QUARTERLY, QUARTERLY),
        (HALF_YEARLY, HALF_YEARLY),
        (YEARLY, YEARLY),
    )
    name = models.CharField(max_length=200)
    amount = MoneyField(max_digits=14, decimal_places=2, default=0.0, validators=[MinMoneyValidator(0)])
    code = models.CharField(default='', max_length=20, blank=True)
    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE)    
    fixed_amount = models.BooleanField(default=False)
    redirect_url = models.URLField(blank=True, null=True)
    description = models.TextField(blank=True)
    extra_info = models.JSONField(null=True, blank=True)
    notify = models.EmailField(blank=True, null=True)
    interval = models.CharField(max_length=100, choices=INTERVAL_CHOICES, default='', blank=True)
    background_image = models.FileField(null=True, blank=True)
    donation_website = models.URLField(default='', blank=True)
    donation_phone = models.CharField(max_length=20, default='', blank=True)
    is_active = models.BooleanField(default=True)
    payment_type = models.CharField(max_length=100, choices=PAYMENT_CHOICES)
    number_of_charges = models.IntegerField(null=True, help_text='Number of times to charge a subscriber, `null` means charge forever')
    completed_charges = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.code = generate_unique_key(klass=PaymentLink, field='code', len=12)
        super().save(*args, **kwargs)

    def __str__(self):
       return self.name



class Checkout(models.Model):
    amount = MoneyField(max_digits=14, decimal_places=2, default=0.0, validators=[MinMoneyValidator(0)])
    code = models.CharField(default='', max_length=20)
    customer = models.ForeignKey('customers.Customer', on_delete=models.SET_NULL, null=True, blank=True)
    meta = models.JSONField(null=True, blank=True)
    trans_reference = models.CharField(max_length=200, default='', blank=True)
    redirect_url = models.URLField(blank=True, default='')

    def save(self, *args, **kwargs):
        if not self.pk:
            self.code = generate_unique_key(klass=Checkout, field='code', len=12)
        super().save(*args, **kwargs)
