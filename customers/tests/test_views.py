from rest_framework.test import APITestCase
from rest_framework import status

from users.tests.factories import UserFactory
from accounts.tests.factories import AccountFactory
from customers.tests.factories import CustomerFactory

class CustomerViewTest(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.account = AccountFactory()
        self.user.accounts.add(self.account)
        self.customer = CustomerFactory(account=self.account)

        
    def test_create_customer(self):
        '''Assert user can create customer '''

        data = {
            'full_name': 'Miracle Alex',
            'email': 'miracle@mirapayments.com',
            'phone': '08022345678'
        }
        url = f'/customers/{self.account.account_number}/'

        self.client.force_authenticate(self.user)       
        resp = self.client.post(url, data) 
        
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(resp.data['full_name'], data['full_name'])
        self.assertEqual(resp.data['email'], data['email'])
        self.assertEqual(resp.data['phone'], data['phone'])

    def test_list_customers(self):
        'Assert that listing all customers for an account works'

        url = f'/customers/{self.account.account_number}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.get(url) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_retrieve_customer(self):
        'Assert that retrieve customer'

        url = f'/customers/customer/{self.customer.id}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.get(url) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_update_customer(self):
        'Assert that update customer works'

        data = {
            'full_name':'Miracle A',
            'email': 'collinsalex50@gmail.com',
            'phone': '08026043569'
        }
        url = f'/customers/customer/{self.customer.id}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.put(url, data) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data['full_name'], data['full_name'])

    def test_partial_update_customer(self):
        'Assert that partial update customer works'

        data = {
            'phone': '08020000009'
        }
        url = f'/customers/customer/{self.customer.id}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.patch(url, data) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data['phone'], data['phone'])