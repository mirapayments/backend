from django.contrib import admin

from transactions.models import Transaction, Card

@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ['short_reference','account','amount_due','transaction_type','status','amount','meta','balance_after_transaction','fee','created_at','updated_at']
    list_filter = ('status', 'transaction_type', 'source', 'service', 'source_identifier')
    search_fields = ['reference', 'account__account_number']
    raw_id_fields = ('account',)
    date_hierarchy = 'created_at'
    # readonly_fields = ['account']

@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ['last_four', 'card_type', 'bank_name', 'is_active', 'country', 'created_at', 'updated_at']
    exclude = ['card_number']
