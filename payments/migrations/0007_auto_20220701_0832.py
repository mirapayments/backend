# Generated by Django 3.2.4 on 2022-07-01 07:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0006_alter_paymentlink_interval'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentlink',
            name='completed_charges',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='paymentlink',
            name='number_of_charges',
            field=models.IntegerField(help_text='Number of times to charge a subscriber, `null` means charge forever', null=True),
        ),
    ]
