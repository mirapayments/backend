import binascii
from os import urandom

from django.db import models
from django.utils import timezone



class AuthTokenManager(models.Manager):
    
    ##############################################
    # PROTECTED CLASS: DO NOT MODIFY!!!
    #############################################

    def create(self, user, account, expiry=None):

        def create_live_token():
            live_token = binascii.hexlify(urandom(20)).decode()
            return live_token

        def create_test_token():
            test_token = binascii.hexlify(urandom(20)).decode()
            return test_token

        if expiry is not None:
            expiry = timezone.now() + expiry

        instance = super(AuthTokenManager, self).create(
            live_token=create_live_token(), test_token=create_test_token(),
            user=user, account=account, expiry=expiry)
        return instance
    

class AuthToken(models.Model):

    ##############################################
    # PROTECTED CLASS: DO NOT MODIFY!!!
    #############################################

    objects = AuthTokenManager()

    live_token = models.CharField(max_length=64, db_index=True)
    user = models.ForeignKey('users.User', related_name='auth_token_set', on_delete=models.CASCADE)
    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    expiry = models.DateTimeField(null=True, blank=True)
    test_token = models.CharField(max_length=64)

    def __str__(self):
        return '%s : %s' % (self.test_token, self.user)

    def renew(self, expiry=None):
        if expiry is not None:
            expiry = timezone.now() + expiry
            
        self.expiry = expiry
        self.test_token = binascii.hexlify(urandom(20)).decode()
        self.live_token = binascii.hexlify(urandom(20)).decode()
        self.save(update_fields=['expiry','test_token','live_token'])
        return self
    
