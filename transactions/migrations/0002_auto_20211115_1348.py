# Generated by Django 3.2.4 on 2021-11-15 12:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transactions', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='meta',
            field=models.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='reference',
            field=models.CharField(blank=True, max_length=100, null=True, unique=True),
        ),
    ]
