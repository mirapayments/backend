from django.http import JsonResponse

from rest_framework import status


def custom_handler_500(request, *args, **kwargs):
    return JsonResponse({
            "status": False,
            "detail": 'An unexpected server error occurred'
        },
        status=status.HTTP_500_INTERNAL_SERVER_ERROR
    )

def custom_handler_404(request, *args, **kwargs):
    return JsonResponse({
            "status": False,
            "detail": 'The requested resource was not found on this server.'
        },
        status=status.HTTP_404_NOT_FOUND
    )