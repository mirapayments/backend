from decimal import  Decimal

#################################
##### DO NOT MODIFY!!!!
################################

# Fees
TRANS_FEES_PERCENT = {
    'NGN': Decimal(0.013),
    'USD': Decimal(0.037),
    'EUR': Decimal(0.037),
    'GBP': Decimal(0.037),
}

TRF_LIMITS = {
    'NGN': Decimal(2000000),
    'USD': Decimal(5000),
    'EUR': Decimal(5000),
    'GBP': Decimal(5000),
}

TRF_MINIMUM = {
    'NGN': Decimal(100),
    'USD': Decimal(1),
    'EUR': Decimal(1),
    'GBP': Decimal(1),
}

TEST_CARDS = ['4000040000400004', '5000050000500005']

# VISA Constants

VISA_TEST_CARD = '4957030420210454'
VISA_BIN = '408999'
VISA_COUNTRY_CODE = '566'
