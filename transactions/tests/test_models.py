from django.test import TestCase

from transactions.tests.factories import TransactionFactory


class TransactionTest(TestCase):

    def test_model_creation(self):
        '''Assert that the Transaction model was created with correct defaults'''

        trans = TransactionFactory()

        self.assertIsNotNone(trans.account) 
        self.assertIsNotNone(trans.source_identifier) 
        self.assertIsNotNone(trans.reference) 
        self.assertIsNotNone(trans.status) 
        self.assertIsNotNone(trans.transaction_type) 