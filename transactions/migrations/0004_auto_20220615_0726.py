# Generated by Django 3.2.4 on 2022-06-15 06:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0004_auto_20220615_0726'),
        ('transactions', '0003_auto_20211120_0002'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction',
            old_name='payment_type',
            new_name='source_identifier',
        ),
        migrations.AddField(
            model_name='transaction',
            name='card',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='transactions.card'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='customer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='customers.customer'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='source',
            field=models.CharField(choices=[('Payment Link', 'Payment Link'), ('Checkout', 'Checkout')], max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='card',
            name='cvv',
            field=models.CharField(blank=True, default='', help_text='e.g. 786', max_length=10),
        ),
        migrations.AlterField(
            model_name='card',
            name='exp_month',
            field=models.CharField(blank=True, default='', help_text='e.g. 06', max_length=50),
        ),
        migrations.AlterField(
            model_name='card',
            name='exp_year',
            field=models.CharField(blank=True, default='', help_text='e.g. 2022', max_length=50),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='meta',
            field=models.JSONField(blank=True, help_text='Any additional data. e.g. "Address":"Lagos, Nigeria"', null=True),
        ),
    ]
