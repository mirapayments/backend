# Generated by Django 3.2.4 on 2022-04-19 07:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0009_auto_20211228_1523'),
    ]

    operations = [
        migrations.AlterField(
            model_name='compliance',
            name='bank_acct_name',
            field=models.CharField(blank=True, default='', max_length=150),
        ),
        migrations.AlterField(
            model_name='compliance',
            name='bank_name',
            field=models.CharField(blank=True, default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='compliance',
            name='bank_number',
            field=models.CharField(blank=True, default='', max_length=15),
        ),
        migrations.AlterField(
            model_name='compliance',
            name='contact_email',
            field=models.EmailField(blank=True, default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='compliance',
            name='contact_number',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='compliance',
            name='identity_file',
            field=models.FileField(blank=True, null=True, upload_to='compliance/'),
        ),
        migrations.AlterField(
            model_name='compliance',
            name='identity_type',
            field=models.CharField(blank=True, default='', max_length=150),
        ),
        migrations.AlterField(
            model_name='compliance',
            name='nationality',
            field=models.CharField(blank=True, default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='compliance',
            name='owner_name',
            field=models.CharField(blank=True, default='', max_length=150),
        ),
    ]
