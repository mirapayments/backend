import datetime as dt

from django.http import HttpRequest

from rest_framework.test import APITestCase

from accounts.tests.factories import  AccountFactory
from transactions.serializers import TransactionSerializer, PaymentLinkTransactionSerializer

class TransactionSerializerTest(APITestCase):

    def test_transaction_serializer(self):
        '''Test Transaction serializer :isvalid() and :create() methods'''
        #TODO: update to add customers, etc
        data = {
            'amount': 100.0,
            'amount_currency': 'USD',
            'description': 'to collect plenty money',
            'source_identifier': 'Single',
            'transaction_type': 'Card'
        }

        trans_serializer = TransactionSerializer(data=data)

        # assert serilizers checks validity correctly
         # TODO: mock visa api call for tests
        self.assertTrue(trans_serializer.is_valid())       

        # assert :create() method works as expected
        account = AccountFactory()
        acc_instance = trans_serializer.save(account=account)
        self.assertEqual(acc_instance.amount.amount, data['amount'])

class PaymentLinkTransactionSerializerTest(APITestCase):

    def test_transaction_serializer(self):
        '''Test Transaction serializer :isvalid() and :create() methods'''
        today = dt.date.today()
        data = {
            'amount': 100.0,
            'amount_currency': 'USD',
            'description': 'to collect plenty money',
            'source_identifier': 'Single',
            'transaction_type': 'Card',
            'service': 'Visa',
            "customer": {
                "full_name": "Miracle Alex",
                "email": "user@example.com"
            },
            "card": {
                "card_number": "4000040000400004",
                "card_type": "Visa",
                "exp_month": today.month + 1,
                "exp_year": today.year,
                "cvv": "678",
                "country": "NGA"
            },
        }
        account = AccountFactory()
        request = HttpRequest()
        trans_serializer = PaymentLinkTransactionSerializer(data=data, context={'account': account, 'request': request})

        # assert serilizers checks validity correctly
        self.assertTrue(trans_serializer.is_valid()) 
       
        # assert :create() method works as expected
        acc_instance = trans_serializer.save()
        self.assertEqual(acc_instance.amount.amount, data['amount'])