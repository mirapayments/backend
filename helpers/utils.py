import string, random
from datetime import timedelta

from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator  
from django.utils import timezone

from rest_framework.serializers import ValidationError
from rest_framework_jwt.settings import api_settings

from users.models import User


def generate_key(len):
    key = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(len))
    return key

def generate_unique_key(klass, field, len=40):
	new_field = generate_key(len)

	if klass.objects.filter(**{field: new_field}).exists():
		return generate_unique_key(klass, field)
	return new_field

def generate_random_number(len):
	return ''.join(random.choice(string.digits) for _ in range(len))

def generate_unique_number(klass, field, len=10):
	new_field = generate_random_number(len)

	if klass.objects.filter(**{field: new_field}).exists():
		return generate_unique_number(klass, field)
	return new_field


def validate_token(uidb64, token):
    is_valid = False

    try:
        id = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(id=id)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user and default_token_generator.check_token(user, token):
        is_valid = True
    
    return user, is_valid


def jwt_token(user):
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


def is_live_mode(request):
    mode = request.headers.get('Mode') or request.META.get('mode')
    if not mode:
        raise ValidationError(detail='Please specify mode')
    if mode == 'live':
        return True
    return False


def last_n_days_ago(n: int):
    return timezone.now().date() - timedelta(days=n)


def get_ip(request):
	'''Get ip address from request'''

	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[0]
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip