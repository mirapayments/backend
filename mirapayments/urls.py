"""mirapayments URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.admin.views.decorators import staff_member_required

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from users.views import  HomeView

handler500 = 'mirapayments.exceptions.custom_handler_500'
handler404 = 'mirapayments.exceptions.custom_handler_404'
schema_view = get_schema_view(
    openapi.Info(
        title="Mirapayments Internal API",
        default_version='v1',
        description="Mirapayments API's for dashboard and services",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,), #IsAdminUser (with group of limited models perms)
)

urlpatterns = [
    path('tower/', admin.site.urls),
    path('users/', include('users.urls')),
    path('accounts/', include('accounts.urls')),
    path('payments/', include('payments.urls')),
    path('transactions/', include('transactions.urls')),
    path('customers/', include('customers.urls')),
    path('logs/', include('logs.urls')),
    path('docs/', schema_view.with_ui(
        'swagger', cache_timeout=0), name='schema-swagger-ui'),
    # path('docs/', staff_member_required(schema_view.with_ui(
    #     'swagger', cache_timeout=0)),name='schema-swagger-ui'),
    path('', HomeView.as_view()),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
