from django.shortcuts import get_object_or_404

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema

from accounts.models import Account
from customers.models import Customer
from customers.serializers import CustomerSerializer
from transactions.serializers import TransactionSerializer


class ListCreateCustomerView(generics.ListCreateAPIView):
    '''
    List or Create customers for an account
    GET or POST /customers/<account_number>/
    account_number - the account number for requesting account
    '''
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    pagination_class = None
    permission_classes = [IsAuthenticated]

    def get_serializer_context(self):
        context = super().get_serializer_context()
        account_number = self.kwargs.get('account_number')
        account = get_object_or_404(Account, account_number=account_number)
        context.update({"account": account})
        return context

    def get_queryset(self):
        account_number = self.kwargs.get('account_number')
        queryset = super().get_queryset().filter(account__account_number=account_number)
        return queryset

    # adding this line for swagger to show docs properly
    @swagger_auto_schema( responses={200: CustomerSerializer})
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    @swagger_auto_schema(request_body=CustomerSerializer, responses={201: CustomerSerializer})
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class RetrieveUpdateCustomerView(generics.RetrieveUpdateAPIView):
    '''
    Get or Update customers
    GET or PUT or PATCH /customers/customer/<pk>/
    pk - the customer id for the customer
    '''
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        trans = self.get_object().transaction_set.all()
        response.data['transactions'] = TransactionSerializer(trans, many=True).data
        return response