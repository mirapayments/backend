from django.contrib import admin

from customers.models import Customer


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['full_name','email','phone','blocked','account','created_at','updated_at']
    list_filter = ['blocked']
    search_fields = ['email', 'full_name']
    raw_id_fields = ('account',)
    date_hierarchy = 'created_at'

