from helpers.api_response import SuccessResponse
from django.shortcuts import get_object_or_404

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from accounts.models import Account
from accounts.views import authorization_header 
from transactions.models import Transaction
from transactions.serializers import TransactionSerializer, PaymentLinkTransactionSerializer, CheckoutTransactionSerializer


source_reference_query = openapi.Parameter('source_reference', openapi.IN_QUERY,
								 description='''The source reference of one or more transactions. E.g /url/?source_refernce=[code].
                                        Here [code] can be retrieved from a paymentlink or other resource''',
								 type=openapi.TYPE_STRING)


class ListTransactionView(generics.ListAPIView):
    '''
    List transactions for an account, optionally list transactions from a source
    GET /transactions/<str:account_number>/
    GET /transactions/<str:account_number>/?source_reference=[code]
    '''
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    pagination_class = None
    permission_classes = [IsAuthenticated]


    def get_queryset(self):
        account_number = self.kwargs.get('account_number')
        queryset = super().get_queryset().filter(account__account_number=account_number).order_by('-created_at')

        source_reference = self.request.query_params.get('source_reference')
        if source_reference:
            queryset = queryset.filter(source_reference=source_reference)

        return queryset

    @swagger_auto_schema(manual_parameters=[authorization_header, source_reference_query],
		operation_id="list account transactions",
		operation_summary='''returns all transactions for an account, can also filter using `source_reference`
        The source reference is the code from paymentlink or checkout''',
        responses={200: TransactionSerializer})
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ExportTransactionView(APIView):
	'''
    Export transactions for an account to a csv file
    POST /transactions/export/<str:account_number>/
    '''
	permission_classes = [IsAuthenticated]
	queryset = Transaction.objects.all()
	http_method_names = ('post',)


	@swagger_auto_schema(
		manual_parameters=[authorization_header],
		operation_id="export account transactions",
		operation_summary="returns an export of all transactions for an account",
		responses={200: 'ok'})
	def post(self, request, account_number):
         from helpers.handlers import ExportItem

         file_type = 'csv'
         data = self.queryset.filter(account__account_number=account_number)
         fields = ['transaction_type','status','reference','source','source_identifier','amount_currency','amount','balance_after_transaction_currency',
            'balance_after_transaction','fee_currency','fee','description','service',
            'service_reference','meta','ip_address','amount_due_currency','amount_due','created_at','updated_at']
         export_item = ExportItem(data, fields=fields)
         return export_item.export(file_type)


class CreatePaymentLinkTransactionView(generics.CreateAPIView):
    '''
    Create transactions for a payment link transaction
    POST /transactions/create/<account_number>/
    '''
    queryset = Transaction.objects.all()
    serializer_class = PaymentLinkTransactionSerializer
    permission_classes = [AllowAny]
    
    @swagger_auto_schema(request_body=PaymentLinkTransactionSerializer, 
		manual_parameters=[authorization_header],
		operation_id="create transaction",
		operation_summary="Create transactions from a payment link with respect to an account",
        responses={201: PaymentLinkTransactionSerializer})
    def post(self, request, account_number):
     
        account = get_object_or_404(Account, account_number=account_number)

        serializer = PaymentLinkTransactionSerializer(data=request.data, context={'account': account, 'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return SuccessResponse(serializer.data)

class CreateCheckoutTransactionView(generics.CreateAPIView):
    '''
    Create transactions for a checkout link transaction
    POST /transactions/charge/<account_number>/ 
    '''
    queryset = Transaction.objects.all()
    serializer_class = CheckoutTransactionSerializer
    permission_classes = [AllowAny]
    
    @swagger_auto_schema(request_body=CheckoutTransactionSerializer, 
		manual_parameters=[authorization_header],
		operation_id="create transaction",
		operation_summary="Create transactions from a checkout link",
        responses={201: CheckoutTransactionSerializer})
    def post(self, request, account_number):
     
        account = get_object_or_404(Account, account_number=account_number)

        serializer = CheckoutTransactionSerializer(data=request.data, context={'request': request, 'account': account})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return SuccessResponse(serializer.data)

class VerifyTransactionView(generics.RetrieveAPIView):
    '''
    Verify status of a transaction [pub]
    GET /transactions/verify/<reference>
    '''
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'reference'

    def get(self, request, *args, **kwargs):

        trans = self.get_object()
        resp = trans.service_verify_trans()
        trans.update_trans_status(resp)
        return super().get(request, *args, **kwargs)


class RetrieveTransactionView(generics.RetrieveAPIView):
    '''
    Retrieve a transaction details [int]
    GET /transactions/transaction/<int:pk>/
    pk - transaction id
    '''
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = [IsAuthenticated]


class CreatePayout(generics.GenericAPIView):
    pass



# TODO
# add test
# add swagger documentation

# implement
# handle timeout transactions
# reverse trans
# push trans



# Just handle checkout in frontend, alongside payment links
# then use checkout model to create standard redirect

# work flow: payments link > redirect to checkout with query params?
# get query params and then redirect to a route without params
# complete checkout (create trans)


# How to obtain acct? pass in the accout_number

# create a server checkout url that leads to a payment
# payment page will use same