from datetime import date

from django.shortcuts import get_object_or_404
from django.conf import settings
from django.db.models import Sum

from rest_framework import generics, status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.parsers import FormParser, MultiPartParser, FormParser
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from helpers.api_response import FailureResponse, SuccessResponse
from helpers.utils import generate_unique_key, is_live_mode, last_n_days_ago
from knox.models import AuthToken
from knox import defaults
from accounts import  serializers as acc_ser 
from accounts.models import  Account, Compliance, TestAccount
from accounts.permissions import IsAccountMember, IsOwnerOrAdmin
from users.models import Role, User
from transactions.models import Transaction



mode_header = openapi.Parameter('mode', openapi.IN_HEADER,
								   description="The request mode "
											   "E.g live",
								   type=openapi.TYPE_STRING)
choice_query = openapi.Parameter('q', openapi.IN_QUERY,
								 description='''options are: today, yesterday, 3days, week, month,
                                                3months, 6months, year, 2years, 3years, all ''',
								 type=openapi.TYPE_STRING)
authorization_header = openapi.Parameter('authorization', openapi.IN_HEADER,
										 description="Token for authenticating a user, E.g Token [token value]",
										 type=openapi.TYPE_STRING)

# file_upload = openapi.Parameter(name="file", in_=openapi.IN_FORM, type=openapi.TYPE_FILE, required=False,
# 								description="Supported files - (csv, xls, xlsx)")
# image_upload = openapi.Parameter(name="image", in_=openapi.IN_FORM, type=openapi.TYPE_FILE, required=False,
# 								 description="Supported images- ('jpg, png, jpeg, webp')")
# video_upload = openapi.Parameter(name="video", in_=openapi.IN_FORM, type=openapi.TYPE_FILE, required=False,
# 								 description="Supported videos- ('3gp, mp4, webm')")

class RetrieveUpdateCompliance(APIView):
    '''
    Retrieve or Update an account compliance info
    GET /accounts/compliance/<str:account_number>/
    PUT /accounts/compliance/<str:account_number>/
    '''
    permission_classes = [IsAuthenticated, IsAccountMember]
    parser_classes = [FormParser, MultiPartParser ]

    @swagger_auto_schema(responses={200: acc_ser.ComplianceSerializer})
    def get(self, request, account_number):
        compliance = Compliance.objects.filter(account__account_number=account_number).first()
        serializer = acc_ser.ComplianceSerializer(compliance)

        return SuccessResponse(data=serializer.data)  

    @swagger_auto_schema(request_body=acc_ser.ComplianceSerializer, responses={200: acc_ser.ComplianceSerializer})
    def put(self, request, account_number):
        compliance =  get_object_or_404(Compliance, account__account_number=account_number)
        serializer = acc_ser.ComplianceSerializer(compliance, request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()

        return SuccessResponse(data=serializer.data)  

class AccountCreate(generics.CreateAPIView):
    '''
    Create a new account
    POST /accounts/create/
    '''
    serializer_class = acc_ser.NewAccountSerializer
    queryset = Account.objects.all()
    pagination_class = None
    permission_classes = [IsAuthenticated]

class AccountTypeList(APIView):
    '''
    List account types, used for dropdown
    GET /accounts/account-types/
    '''
    permission_classes = [AllowAny]

    def get(self, request):
        accounts = [x[0] for x in Account.ACCOUNT_CHOICES]
        return SuccessResponse(data={'choices': accounts})

class CurrencyList(APIView):
    '''
    List accepted currencies
    GET /accounts/account-types/
    '''
    permission_classes = [AllowAny]

    def get(self, request):
        currency = [x[0] for x in settings.CURRENCY_CHOICES]
        return SuccessResponse(data={'choices': currency})

class GenerateCredentials(APIView):
    permission_classes = [IsAuthenticated, IsOwnerOrAdmin]

    def get(self, request, account_number):
        '''
        Get existing API credentials 
        GET /accounts/credentials/<str:account_number>/
        '''
        # retrieve account, token and get public key
        account = get_object_or_404(Account, account_number=account_number)
        token = get_object_or_404(AuthToken, account=account)
        testaccount = get_object_or_404(TestAccount, account_number=account_number)

        data = {
            'live_token': '{}{}'.format(defaults.LIVE_KEY_PREFIX, token.live_token),
            'test_token': '{}{}'.format(defaults.TEST_KEY_PREFIX, token.test_token),
            'live_pub_key': '{}{}'.format(defaults.LIVE_PUBLIC_KEY_PREFIX, account.live_pk),
            'test_pub_key': '{}{}'.format(defaults.TEST_PUBLIC_KEY_PREFIX, testaccount.test_pk),
            'test_webhook_url': testaccount.test_webhook_url,
            'live_webhook_url': account.live_webhook_url
        }
        return SuccessResponse(data=data)


    def post(self, request, account_number):
        '''
        Save Web hook urls 
        POST /accounts/credentials/<str:account_number>/
        '''
        # retrieve post data
        live_webhook_url = request.data.get('live_webhook_url')
        test_webhook_url = request.data.get('test_webhook_url')

        # retrieve account, token and update webhook urls
        account = get_object_or_404(Account, account_number=account_number)
        account.live_webhook_url = live_webhook_url
        account.save(update_fields=['live_webhook_url'])

        token = get_object_or_404(AuthToken, account=account)

        testaccount = get_object_or_404(TestAccount, account_number=account_number)
        testaccount.test_webhook_url = test_webhook_url
        testaccount.save(update_fields=['test_webhook_url'])


        data = {
            'live_token': '{}{}'.format(defaults.LIVE_KEY_PREFIX, token.live_token),
            'test_token': '{}{}'.format(defaults.TEST_KEY_PREFIX, token.test_token),
            'live_pub_key': '{}{}'.format(defaults.LIVE_PUBLIC_KEY_PREFIX, account.live_pk),
            'test_pub_key': '{}{}'.format(defaults.TEST_PUBLIC_KEY_PREFIX, testaccount.test_pk),
            'test_webhook_url': testaccount.test_webhook_url,
            'live_webhook_url': account.live_webhook_url
        }
        return SuccessResponse(data=data)

class RefreshKeys(APIView):
    permission_classes = [IsAuthenticated, IsOwnerOrAdmin]

    def post(self, request, account_number):
        '''
        Generate new API credentials
        POST /accounts/credentials/<str:account_number>/
        '''
    
        # retrieve account, token and update keys
        account = get_object_or_404(Account, account_number=account_number)
        account.public_key = generate_unique_key(klass=Account, field='public_key', len=40)
        account.save(update_fields=['public_key'])

        token = get_object_or_404(AuthToken, account=account)
        token = token.renew()

        testaccount = get_object_or_404(TestAccount, account_number=account_number)
        testaccount.public_key = generate_unique_key(klass=TestAccount, field='public_key', len=40)
        testaccount.save(update_fields=['public_key'])

        data = {
            'live_token': '{}{}'.format(defaults.LIVE_KEY_PREFIX, token.live_token),
            'test_token': '{}{}'.format(defaults.TEST_KEY_PREFIX, token.test_token),
            'live_pub_key': '{}{}'.format(defaults.LIVE_PUBLIC_KEY_PREFIX, account.live_pk),
            'test_pub_key': '{}{}'.format(defaults.TEST_PUBLIC_KEY_PREFIX, testaccount.test_pk),
            'test_webhook_url': testaccount.test_webhook_url,
            'live_webhook_url': account.live_webhook_url
        }
        return SuccessResponse(data=data)


class RemoveUser(APIView):
    '''
    Endpoint to remove a user from an account, requesting user must be account owner or admin
    GET /accounts/remove-user/<str:account_number>/<int:user_id>/
    '''
    permission_classes = [IsAuthenticated, IsOwnerOrAdmin]

    def post(self, request, account_number, user_id):
        account = get_object_or_404(Account, account_number=account_number)
        user_to_be_removed = get_object_or_404(User, id=user_id)
        account_roles = Role.objects.filter(account=account)

        if user_to_be_removed == request.user:
            return FailureResponse(detail='You cannot remove yourself from an account', status=status.HTTP_403_FORBIDDEN)

        if not account in user_to_be_removed.accounts.all():
            return FailureResponse(detail='This user is not a member of this account', status=status.HTTP_404_NOT_FOUND)

        user_to_be_removed_role = account_roles.filter(user=user_to_be_removed).first()

        if user_to_be_removed_role and (user_to_be_removed_role.role == Role.OWNER):
            return FailureResponse(detail='You can not remove the owner of an account, please contact us if you really need to do this', status=status.HTTP_403_FORBIDDEN)

        user_to_be_removed.accounts.remove(account)
        user_to_be_removed_role.delete()
        # TODO: send a mail to this user
        return SuccessResponse()


class AccountDetailView(APIView):
    '''
    Account Detail API - returns some info about an account
    GET /accounts/detail/<str:account_number>/
    '''
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(manual_parameters=[mode_header, authorization_header],
        responses={200: acc_ser.AccountSerializer})
    def get(self, request, account_number):
        
        if is_live_mode(request):
            account = get_object_or_404(Account, account_number=account_number)

            # return error if account is deactivated
            if not account.is_active:
                return FailureResponse(detail='This account has been deactivated')
        else:
            account = get_object_or_404(TestAccount, account_number=account_number)

        data = acc_ser.AccountSerializer(account).data
        # use account_numer to retrieve role instead of instance, as instance can be `account` or `testaccount`
        role = Role.objects.get(account__account_number=account.account_number, user=request.user)
        data['signed_in_user_role'] = role.role 
        return SuccessResponse(data=data)


class BalanceView(generics.GenericAPIView):
    '''
    Balance API - returns some info about an account balance
    GET /accounts/balance/<str:account_number>/
    '''
    permission_classes = [IsAuthenticated]
    serializer_class = acc_ser.BalanceSerializer
    queryset = Account.objects.all()
    lookup_field = 'account_number'

    @swagger_auto_schema(manual_parameters=[mode_header]) 
    def get(self, request, account_number):
        account = self.get_object()
        data = self.get_serializer(account).data
        return SuccessResponse(data)

class DashboardView(APIView):
    '''
    Dashboard API - returns account statistics
    GET /accounts/dashboard/<str:account_number>/
    filter ?q=<options>
    '''
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(manual_parameters=[choice_query])
    def get(self, request, account_number):
        present_options = {
            'today': last_n_days_ago(0),
            'yesterday': last_n_days_ago(1),
            '3days': last_n_days_ago(3),
            'week': last_n_days_ago(7),
            'month': last_n_days_ago(30),
            '3months': last_n_days_ago(90),
            '6months': last_n_days_ago(180),
            'year': last_n_days_ago(365),
            '2years': last_n_days_ago(2*365),
            '3years': last_n_days_ago(3*365),
        }
        past_options = {
            'today': last_n_days_ago(1),
            'yesterday': last_n_days_ago(2*1),
            '3days': last_n_days_ago(2*3),
            'week': last_n_days_ago(2*7),
            'month': last_n_days_ago(2*30),
            '3months': last_n_days_ago(2*90),
            '6months': last_n_days_ago(2*180),
            'year': last_n_days_ago(2*365),
            '2years': last_n_days_ago(4*365),
            '3years': last_n_days_ago(6*365),
        }
        q = request.query_params.get('q', 'all')
        present_query = present_options.get(q)
        past_query = past_options.get(q)
        trans = Transaction.objects.filter(account__account_number=account_number)

        if q == 'all':
            present_trans = trans.all()
            past_trans = trans.all()
        else:
            present_trans = trans.filter(created_at__gte=present_query)
            past_trans = trans.filter(created_at__range=[past_query, present_query])

        account = get_object_or_404(Account, account_number=account_number)

        present_successful_trans_count = present_trans.filter(status=Transaction.SUCCESS).count()
        present_failed_trans_count = present_trans.filter(status=Transaction.FAILED).count()
        present_customer_count = present_trans.distinct('customer').count()

        past_successful_trans_count = past_trans.filter(status=Transaction.SUCCESS).count()
        past_failed_trans_count = past_trans.filter(status=Transaction.FAILED).count()
        past_customer_count = past_trans.distinct('customer').count()

        percentage_succesful = round(((present_successful_trans_count - past_successful_trans_count) / max([past_successful_trans_count, 1])) * 100)
        percentage_failed = round(((present_failed_trans_count - past_failed_trans_count) / max([past_failed_trans_count, 1])) * 100)
        percentage_customers = round(((present_customer_count - past_customer_count) / max([past_customer_count, 1])) * 100)

        successful_trans = {
            'value': present_successful_trans_count,
            'percentage': '{}%'.format(percentage_succesful),
            'arrow': 'up' if present_successful_trans_count >= past_successful_trans_count else 'down'
        }
        failed_trans = {
            'value': past_failed_trans_count,
            'percentage': '{}%'.format(percentage_failed),
            'arrow': 'up' if present_failed_trans_count >= past_failed_trans_count else 'down'
        }
        customers = {
            'value': present_customer_count,
            'percentage': '{}%'.format(percentage_customers),
            'arrow': 'up' if present_customer_count >= past_customer_count else 'down'
        }

        #TODO: filter inflow, outflow correctly
        inflow_trans = present_trans.exclude(transaction_type=Transaction.ACCOUNT)
        outflow_trans = present_trans.filter(transaction_type=Transaction.ACCOUNT)

        inflow_amounts = []
        inflow_timestamps = []
        for tran in inflow_trans:
            inflow_amounts.append(tran.amount.amount)
            inflow_timestamps.append(tran.created_at.strftime('%d %b'))
        inflow_stats = {'amounts': inflow_amounts, 'timestamps': inflow_timestamps}

        outflow_amounts = []
        outflow_timestamps = []
        for tran in outflow_trans:
            outflow_amounts.append(tran.amount.amount)
            outflow_timestamps.append(tran.created_at.strftime('%d %b'))
        outflow_stats = {'amounts': outflow_trans, 'timestamps': outflow_timestamps}

        total_inflow = inflow_trans.aggregate(Sum('amount'))['amount__sum'] or 0.0
        total_outflow = outflow_trans.aggregate(Sum('amount'))['amount__sum'] or 0.0
        total_trans = present_trans.aggregate(Sum('amount'))['amount__sum'] or 0.0

        data = {
            'sucessful_transactions': successful_trans,
            'failed_transactions': failed_trans,
            'customers': customers,
            'inflow_stats': inflow_stats,
            'outflow_stats': outflow_stats,
            'total_inflow': total_inflow,
            'total_outflow': total_outflow,
            'total_trans': total_trans,
            'balance_details': {
                'balance': account.balance.amount,
                'balance_currency': account.balance_currency,
            }
        }
        return SuccessResponse(data=data)