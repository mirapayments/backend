from django.test import TestCase
from django.urls import reverse

from users.tests.factories import UserFactory
from payments.tests.factories import PaymentLinkFactory


class PaymentLinkAdminTest(TestCase):

    def setUp(self):
        self.user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login(self.user)
        self.payment_link = PaymentLinkFactory()

    def test_changelist_view(self):
        '''Assert payment link change list view loads well'''

        url = reverse("admin:%s_%s_changelist" % (self.payment_link._meta.app_label, self.payment_link._meta.model_name))
        page = self.client.get(url)
        self.assertEqual(page.status_code, 200)

    def test_change_view(self):
        '''Assert payment link change view page opens successfully'''

        url = reverse("admin:%s_%s_change" % (self.payment_link._meta.app_label, self.payment_link._meta.model_name), args=(self.payment_link.pk,))
        page = self.client.get(url)
        self.assertEqual(page.status_code, 200)