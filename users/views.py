from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.shortcuts import get_object_or_404

from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema

from accounts.models import Account
from accounts.serializers import AccountSerializer
from accounts.permissions import IsOwnerOrAdmin
from accounts.views import mode_header
from helpers.mailers import send_verification_email, send_password_reset_mail
from helpers.api_response import SuccessResponse, FailureResponse
from helpers.utils import validate_token, jwt_token
from users import serializers as ser
from users.models import  User
from users.signals import reset_password_token_created

class LoginView(APIView):
    '''
    Login users
    POST /users/login/
    '''
    permission_classes = [AllowAny]

    @swagger_auto_schema(request_body=ser.UserLoginSerializer, responses={200: ser.UserSerializer} )
    def post(self, request):
        # print(request.headers.get('Mode'))
        serializer = ser.UserLoginSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            email = serializer.validated_data.get("email")
            password = serializer.validated_data.get("password")
            account_number = serializer.validated_data.get("account_number")

        user = authenticate(email=email, password=password)
        if not user:
            return FailureResponse(
				detail='Invalid credentials',
				status=status.HTTP_401_UNAUTHORIZED
            )
        if not user.email_verified:
            return FailureResponse(
                detail='Please verify your email address',
                status=status.HTTP_403_FORBIDDEN
            )
        login(request, user)

        user_logged_in.send(sender=request.user.__class__,
            request=request, user=request.user)    
        data = ser.UserSerializer(user, context={'request': request}).data
        
        data['token'] = jwt_token(user)

        return SuccessResponse(detail='Login successful', data=data)


class LogoutView(APIView):
    '''
    Log out users
    POST /users/logout/
    '''
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(responses={204: None} )
    def post(self, request, format=None):
        # request._auth.delete()
        logout(request)
        user_logged_out.send(sender=request.user.__class__,
                             request=request, user=request.user)
        return SuccessResponse(None, status=status.HTTP_204_NO_CONTENT)
        

class SignUpView(APIView):
    '''
    Create new user accounts
    POST /users/signup/
    '''
    permission_classes = [AllowAny]

    @swagger_auto_schema(request_body=ser.UserSerializer, responses={201: ser.UserSerializer} )
    def post(self, request):
        serializer = ser.UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
                 
        send_verification_email(user)
        return SuccessResponse(
                detail='User created successfully',
                data=serializer.data,
                status=status.HTTP_201_CREATED
            )
				

class UserDetailUpdateView(generics.GenericAPIView):
    """
    Retrieve users and update user details
    GET /users/me/
    PUT /users/me/
    """
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated]
    pagination_class = None

    @swagger_auto_schema(responses={200: ser.UserSerializer} )
    def get(self, request, *args, **kwargs):
        data = ser.UserSerializer(instance=request.user).data
        return SuccessResponse(data=data)

    @swagger_auto_schema(request_body=ser.UserUpdateSerializer, responses={200: ser.UserSerializer} )
    def put(self, request, *args, **kwargs):
        serializer = ser.UserUpdateSerializer(request.user, request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        updated_user = serializer.save()
        return SuccessResponse(
            detail='User updated successfully',
            data=ser.UserSerializer(updated_user).data,
        )


class SendVerificationEmail(APIView):
    '''
    Send verification email
    POST /users/send-verification-email/
    '''
    permission_classes = [AllowAny]

    def post(self, request):
        email = request.data.get('email')
        if not email:
            return FailureResponse(detail='Please provide an email')

        user = User.objects.filter(email=email)
        if user.exists(): 
            send_verification_email(user.first())
        return SuccessResponse(detail='Email sent successfully')


class VerifyEmail(APIView):
    '''
    Verify the email
    GET /users/verify-email/<uidb64>/<token>/
    '''
    permission_classes = [AllowAny]
    
    def get(self, request, uidb64, token):

        user, is_valid = validate_token(uidb64, token)
  
        if user and is_valid:

            if user.email_verified:
                return SuccessResponse(detail='Your email has already been verified')
            
            user.email_verified = True
            user.save()
            return SuccessResponse(detail='Email verification was successful')

        else:
            return FailureResponse(detail='Email verification failed. Token is either invalid or expired')


class ResetPasswordRequest(APIView):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        """
        An api view which provides a method to request a password reset token based on an e-mail address
        Sends a signal reset_password_token_created when a reset token was created.
        POST /users/password-reset-request/
        """

        email = request.data.get('email')
        if not email:
            return FailureResponse(detail='Email is required')

        # find a user by email address (case insensitive search)
        user = User.objects.filter(email__iexact=email)

        if user.exists() and getattr(user.first(), 'is_active', False):
            send_password_reset_mail(user.first())            
            # send a signal that the password token was created
            reset_password_token_created.send(sender=self.__class__, user=user.first())
        return SuccessResponse(detail='Kindly check your email to set your password')


class ResetPasswordValidateToken(APIView):
    """
    An api view which provides a method to verify that a token is valid
    POST /users/reset-password-validate-token/
    """
    permission_classes = [AllowAny]
    serializer_class = ser.ValidateTokenSerializer

    @swagger_auto_schema(request_body=ser.ValidateTokenSerializer )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return SuccessResponse(detail="Token is valid")


class ResetPasswordConfirm(APIView):
    """
    An Api View which provides a method to reset a password based on a unique token
    POST /users/reset-password-confirm/
    """
    permission_classes = [AllowAny]
    serializer_class = ser.ResetPasswordConfirmSerializer

    @swagger_auto_schema(request_body=ser.ResetPasswordConfirmSerializer )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.reset_password()
        return SuccessResponse(detail="Password reset was successful")

    
class UserInvitationView(APIView):
    '''
    Invite account user
    POST /users/invite/<str:account_number>/
    '''
    permission_classes = [IsAuthenticated, IsOwnerOrAdmin]

    @swagger_auto_schema(request_body=ser.UserInvitationSerializer )
    def post(self, request, account_number):
        account = get_object_or_404(Account, account_number=account_number)
        serializer = ser.UserInvitationSerializer(data=request.data, context={'account': account, 'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.invite()

        return SuccessResponse(detail='Invite has been sent to new user')


class ListAccountUsersView(APIView):
    '''
    List all users tied to an account
    GET /users/list/<str:account_number>/
    '''
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(responses={200: ser.UserSerializer} )
    def get(self, request, account_number):
        account = get_object_or_404(Account, account_number=account_number)
        users = account.user_set.all()
        data = ser.UserSerializer(instance=users, many=True, context={'account': account}).data
        return SuccessResponse(data=data)


class ListUserAccountsView(APIView):
    ''''
    List all accounts tied to a user
    GET /users/me/accounts/
    '''
    permission_classes = [IsAuthenticated]


    @swagger_auto_schema(responses={200: AccountSerializer} )
    def get(self, request):
        accounts = request.user.accounts.all()
        data = AccountSerializer(instance=accounts, many=True).data
        return SuccessResponse(data=data)


class ActivateInvitedUser(APIView):
    '''
    Add an invited user
    POST /users/activate/
    '''
    permission_classes = [AllowAny]

    @swagger_auto_schema(request_body=ser.ActivateInvitedUserSerializer )
    def post(self, request):
        serializer = ser.ActivateInvitedUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.activate()
        
        return SuccessResponse(detail='User successfully activated')

class HomeView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        data = {
            "service": "Mirapayments API",
            "version": "1.0"
        }
        detail = "Are you sure this is where you want to be?"
        return SuccessResponse(detail=detail, data=data)

class ChangePassword(APIView): 
    '''
    User change password
    PUT /users/me/change-password/
    '''
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(request_body=ser.ChangePasswordSerializer )
    def put(self, request):        
        serializer = ser.ChangePasswordSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.apply_password()

        return SuccessResponse(detail='Password changed successfully')



class CICDVerifyEmail(APIView):
    '''
    Verify the email
    GET /users/cicd-verify-email/<str:email>/
    '''
    permission_classes = [AllowAny]
    
    def get(self, request, email):
        user = User.objects.filter(email__iexact=email).first()
        if user:

            if user.email_verified:
                return SuccessResponse(detail='Your email has already been verified')
            
            user.email_verified = True
            user.save()
            return SuccessResponse(detail='Email verification was successful')

        else:
            return FailureResponse(detail='Email verification failed. User may not exist', status=status.HTTP_404_NOT_FOUND)


class ContactUsView(APIView):
    '''
    Contact us form 
    POST /users/contact-us
    '''
    permission_classes = [AllowAny]

    @swagger_auto_schema(request_body=ser.ContactUsSerializer )
    def post(self, request):
        serializer = ser.ContactUsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.send()

        return SuccessResponse(data=serializer.data, detail='Email sent successfully')
