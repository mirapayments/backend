import factory

from users.models import Role


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'users.User'

    first_name = factory.Sequence(lambda n: 'user{}'.format(n))
    email = factory.LazyAttribute(lambda obj: '%s@mirapayments.com' % obj.first_name)
    phone = factory.Sequence(lambda n: '0801234567{}'.format(n))
    password = factory.Sequence(lambda n: 'password{}'.format(n))
    is_superuser = False
    is_staff = False
    email_verified = False
    accounts = factory.RelatedFactory('accounts.tests.factories.AccountFactory')

class RoleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'users.Role'

    user = factory.SubFactory('users.tests.factories.UserFactory')
    account = factory.SubFactory('accounts.tests.factories.AccountFactory')
    role = factory.Iterator([x[0] for x in Role.ROLE_CHOICES])
