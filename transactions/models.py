from decimal import Decimal

from django.db import models

from djmoney.models.fields import MoneyField
from djmoney.models.validators import MinMoneyValidator
from djmoney.money import Money

from helpers.utils import generate_unique_number
from helpers import choices
from helpers import constants
from services.visa import Visa


class Transaction(models.Model):
    '''
    The model to store Transactions

    - transaction_type: the type of transaction
    - reference: the unique code for transactions
    - amount: the amount of the payment link, managed with django-money
    - account: the account which the transaction happened
    - customer: customer in a transaction
    - balance_after_transaction: the balance after the transaction
    - fee: tthe fee paid by the receiver (account owner)
    - meta: extra info on the transaction
    - amount_due: the amount due for account
    - description: the transaction description
    - service_reference: the reference provided by the service, e.g Visa's trans reference
    - service: the service used to facilitate transaction
    - service_response - status of transaction from service or gateway, can be read from status code or response msgs
    - full_service_response - full response of the service
    - ip_address: the ip address of device where transaction occured
    - source: the source of payment for a transaction
    - source_identifier: the identifier of the source of payment for a transaction
    - source_reference: the reference of the trans source, e.g payment link code, checkout code
    - created_at: time transaction was created
    - updated_at: time transaction was updated
    '''
    INITIATED, SUCCESS, FAILED, CANCELED, REVERSED = 'Initiated', 'Success', 'Failed', 'Canceled', 'Reversed'    
    TRANSACTION_STATUS = (
        (INITIATED, INITIATED),
        (SUCCESS, SUCCESS),
        (FAILED, FAILED),
        (CANCELED, CANCELED),
        (REVERSED, REVERSED)
    )
    SINGLE_LINK, SUBSCRIPTION_LINK, DONATION_LINK, PRODUCT_LINK = 'Single', 'Subscription', 'Dontation', 'Product'
    SOURCE_IDENTIFIER_TYPES = (
        (SINGLE_LINK, SINGLE_LINK),
        (SUBSCRIPTION_LINK, SUBSCRIPTION_LINK),
        (DONATION_LINK, DONATION_LINK),
        (PRODUCT_LINK, PRODUCT_LINK)
    )
    PAYMENTLINK, CHECKOUT = 'Payment Link', 'Checkout'
    SOURCE_CHOICES = (
        (PAYMENTLINK, PAYMENTLINK),
        (CHECKOUT, CHECKOUT),
    )
    CARD, BANK, BANK_TRANSFER, USSD, ACCOUNT = 'Card', 'Bank', 'BankTransfer', 'USSD', 'Account'
    TRANSACTION_TYPES = (
        (CARD, CARD),
        (BANK, BANK),
        (BANK_TRANSFER, BANK_TRANSFER),
        (USSD, USSD),
    )
    VISA, MASTERCARD, VERVE, AMERICAN_EXPRESS = 'Visa', 'Mastercard', 'Verve', 'American Express'
    SERVICE_CHOICES = (
        (VISA, VISA),
        (MASTERCARD, MASTERCARD),
        (VERVE, VERVE),
        (AMERICAN_EXPRESS, AMERICAN_EXPRESS),
    )

    account = models.ForeignKey('accounts.Account', on_delete=models.SET_NULL, blank=True, null=True)
    transaction_type = models.CharField(max_length=100, choices=TRANSACTION_TYPES, default='')
    status = models.CharField(max_length=50, choices=TRANSACTION_STATUS, default=INITIATED)
    reference = models.CharField(max_length=100, unique=True, null=True, blank=True)
    source_identifier = models.CharField(max_length=100, choices=SOURCE_IDENTIFIER_TYPES, default='', blank=True)
    source = models.CharField(max_length=100, choices=SOURCE_CHOICES, default='', blank=True)
    source_reference = models.CharField(max_length=100, default='', blank=True, help_text='the reference of the trans source, e.g payment link code, checkout code')
    customer = models.ForeignKey('customers.Customer', null=True, blank=True, on_delete=models.SET_NULL)
    amount = MoneyField(max_digits=12, decimal_places=2, default=0.0, validators=[MinMoneyValidator(0)])
    balance_after_transaction = MoneyField(max_digits=12, default=0.0, decimal_places=2, blank=True)
    fee = MoneyField(max_digits=12, default=0.0, decimal_places=2, validators=[MinMoneyValidator(0)], blank=True)
    amount_due = MoneyField(max_digits=12, default=0.0, decimal_places=2, validators=[MinMoneyValidator(0)], blank=True)
    description = models.TextField(null=True, blank=True)
    meta = models.JSONField(null=True, blank=True, help_text='Any additional data. e.g. "Address":"Lagos, Nigeria"')
    service_reference = models.CharField(max_length=200, default='', blank=True)
    service = models.CharField(max_length=100, choices=SERVICE_CHOICES, default='', blank=True)
    service_response = models.CharField(max_length=100, default='', blank=True)
    full_service_response = models.JSONField(null=True, blank=True)
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    card = models.ForeignKey('transactions.Card', null=True, blank=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


    def __str__(self):
        return "ref: {}, status: {}, amount: {}".format(self.reference, self.status, self.amount)

    def short_reference(self):
        return f'{self.reference[:10]}...'

    def process_fees(self):
        '''
        Calculates trans fees dynamically using the trans amount and default currency fees
        '''
        fee_percent = constants.TRANS_FEES_PERCENT.get(self.amount_currency)
        fee = round(self.amount.amount * fee_percent, 2)
        return Money(fee, self.amount_currency)
    
    def call_service(self):
        from services.visa import get_visa_pull_data

        if self.service == self.VISA:
            data = get_visa_pull_data(self)
            resp = Visa().pull_funds(data)
        return resp
    
    def service_verify_trans(self, bin=None):
        if self.service == self.VISA:
            resp = Visa().query_api(bin=bin, transactionIdentifier=self.service_reference)
        return resp

    def update_trans_status(self, resp):
        resp_json = resp.json()

        if isinstance(resp_json, list):
            resp_json = resp_json[0]

        if self.service == self.VISA:
            if (resp_json.get('actionCode') == '00') and (resp_json.get('statusCode') == 'COMPLETED'):
                self.status = self.SUCCESS
            else:
                self.status = self.FAILED
            
        self.save()
        return self

    def succeed(self):
        if self.status in [self.SUCCESS, self.FAILED]:
            return

        if (self.transaction_type in ['Card', 'Bank', 'BankTransfer', 'USSD']):
            fee = self.process_fees()
            self.amount_due = self.amount - fee
            self.fee = fee

            if self.card.card_number in constants.TEST_CARDS:
                self.status = self.SUCCESS
            
            else:
                # call service endpoint
                resp = self.call_service()
                response_json = resp.json()
                action_code = response_json.get('actionCode')
                print("Service Response:", response_json)
                if resp.ok and (action_code in ['00']):
                # if True:
                    # print('amount_due:', self.amount_due)
                    # print('balance:', self.account.balance)

                    # import pdb; pdb.set_trace()
                    self.account.credit(self.amount_due)
                    self.status = self.SUCCESS
                    self.service_response = choices.VISA_ACTION_CODES.get(action_code)
                    self.service_reference = response_json.get('transactionIdentifier')
                    self.full_service_response = response_json
                else:
                    self.status = self.FAILED
                    self.account.credit(Money(0, self.account.balance_currency))
                    self.service_response = choices.VISA_ACTION_CODES.get(action_code, '')
                    self.service_reference = response_json.get('transactionIdentifier', '')
                    self.full_service_response = response_json

        self.balance_after_transaction = self.account.balance
        self.save()
        # transaction_notification(self)
        return self

    def reject(self):
        if (self.status == self.SUCCESS) or (self.status == self.FAILED):
            return
        if (self.transaction_type in ['Card', 'Bank', 'BankTransfer', 'USSD']):
            self.account.balance -= self.amount_due
            self.account.save()
        self.balance_after_transaction = self.account.balance
        self.status = self.FAILED
        self.save()
        # notify_transaction_failed(self)
        return self

	# def process(self):
	# 	account = self.account
	# 	self.total = Decimal(self.amount) + Decimal(self.fee)
	# 	if (self.status != TransactionOptions.SUBMITTED):
	# 		return
	# 	if (self.type_of_transaction != TransactionOptions.Deposit):
	# 		account.balance -= self.total
	# 		account.save()
	# 	self.status = TransactionOptions.PROCESSING
	# 	self.balance_after_transaction = account.balance
	# 	self.save()


    def save(self, *args, **kwargs):
	    if not self.pk:
		    self.reference = generate_unique_number(Transaction, 'reference', len=32)
	    super().save(*args, **kwargs)


class Card(models.Model):
    VISA, MASTERCARD, VERVE, AMERICAN_EXPRESS = 'Visa', 'Mastercard', 'Verve', 'American Express'
    CARD_CHOICES = (
        (VISA, VISA),
        (MASTERCARD, MASTERCARD),
        (VERVE, VERVE),
        (AMERICAN_EXPRESS, AMERICAN_EXPRESS),
    )
    
    card_number = models.CharField(max_length=100, blank=True, default='')
    bank_name = models.CharField(default='', blank=True, max_length=50)
    card_type = models.CharField(max_length=50, blank=True, default='', choices=CARD_CHOICES)
    exp_month = models.CharField(max_length=50, blank=True, default='', help_text='e.g. 06')
    exp_year = models.CharField(max_length=50, blank=True, default='', help_text='e.g. 2022')
    auth_code = models.CharField(max_length=50, blank=True, default='') 
    cvv = models.CharField(max_length=10, blank=True, default='', help_text='e.g. 786')
    is_active = models.BooleanField(default=True)
    country = models.CharField(max_length=150, choices=choices.ISO_COUNTRY_CODES, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s Card: ************%s' % (self.card_type, self.card_number[12:])

    def first_five(self):
        return self.card_number[:5]

    def last_four(self):
        return self.card_number[-4:]


# class TransactionMetric(Transaction):
# 	class Meta:
# 		proxy = True
# 		verbose_name_plural = 'Transaction Metrics'  
