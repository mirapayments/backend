#!/bin/bash
set -x
set -eu
set -o pipefail

export AWS_ACCESS_KEY_ID=$AWS_DEV_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_DEV_SECRET_ACCESS_KEY

apt-get update && apt-get install -y python3-pip 
# apt update && apt install -y python3-pip 
pip3 install -U awscli 
pip3 install docker-compose==1.29.2


docker login -u AWS -p $(aws ecr get-login-password --region us-east-2) $AWS_ACCT_ID.dkr.ecr.us-east-2.amazonaws.com

# docker-compose -f docker-compose-ci.yml build --pull
docker-compose -f docker-compose-ci.yml build 

docker-compose -f docker-compose-ci.yml push 
