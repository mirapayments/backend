from django.test import TestCase

from customers.tests.factories import CustomerFactory


class CustomerTest(TestCase):

    def test_model_creation(self):
        '''Assert that the Customer model was created with correct defaults'''

        customer = CustomerFactory()

        self.assertIsNotNone(customer.full_name) 
        self.assertIsNotNone(customer.email) 
        self.assertIsNotNone(customer.phone) 
        self.assertFalse(customer.blocked) 
