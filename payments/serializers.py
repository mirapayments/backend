from django.conf import settings

from rest_framework import serializers
from djmoney.contrib.django_rest_framework import MoneyField
from djmoney.money import Money

from knox.models import AuthToken
from helpers import constants
from payments.models import Checkout, PaymentLink
from customers.serializers import CustomerSerializer
from customers.models import Customer


class PaymentLinkSerializer(serializers.ModelSerializer):
    account = serializers.CharField(source='account.account_number',  read_only=True)
    fees_payer = serializers.CharField(source='account.fees_payer',  read_only=True)
    amount = MoneyField(max_digits=14, decimal_places=2, min_value=0)
    amount_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES)
    code = serializers.CharField(read_only=True)
    is_active = serializers.BooleanField(default=True)
    
    class Meta:
        model = PaymentLink
        fields = '__all__'
        read_only_fields = ['completed_charges']
    
    def validate(self, attrs):
        attrs = super().validate(attrs)
        amount = attrs.get('amount')
        amount_currency = attrs.get('amount_currency')

        if amount < Money(constants.TRF_MINIMUM[amount_currency], amount_currency):
            raise serializers.ValidationError('Minimum transaction amount is {}'.format(constants.TRF_MINIMUM[amount_currency]))
        return attrs

        
class CheckoutSerializer(serializers.ModelSerializer):
    amount = MoneyField(max_digits=14, decimal_places=2, min_value=0)
    amount_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES)
    checkout_link = serializers.SerializerMethodField(read_only=True)
    customer = CustomerSerializer()
    
    class Meta:
        model = Checkout
        fields = '__all__'
    
    def validate(self, attrs):
        attrs = super().validate(attrs)
        amount = attrs.get('amount')
        amount_currency = attrs.get('amount_currency')

        if self.context['request'].successful_authenticator.model is not AuthToken:
            raise serializers.ValidationError(detail='Authentication method not allowed')
            
        if amount.currency != self.context['request'].auth.account.balance.currency:
            raise serializers.ValidationError('Please use the currency attached to your account')

        if amount < Money(constants.TRF_MINIMUM[amount_currency], amount_currency):
            raise serializers.ValidationError('Minimum transaction amount is {}'.format(constants.TRF_MINIMUM[amount_currency]))
        
        return attrs
    
    def get_checkout_link(self, obj):
        return "http://checkout.mirapayments.com/pay/{}".format(obj.code)

    def create(self, validated_data):
        customer_data = validated_data.pop('customer')
        customer_data['account'] = self.context['request'].auth.account
        validated_data['customer'], _ = Customer.objects.get_or_create(**customer_data)

        checkout = super().create(validated_data)

        return checkout


# validate checkout amount here
# validate amount in respect to currenvy -- payment link, checkout, trans