import datetime as dt

from django.conf import settings
from django.db import transaction

from rest_framework import serializers
from djmoney.contrib.django_rest_framework import MoneyField
from djmoney.money import Money

from helpers import constants
from helpers import choices
from helpers import utils
from customers.models import Customer
from customers.serializers import CustomerSerializer
from transactions.models import Card, Transaction
from services.visa import Visa
from payments.models import Checkout


class CardSerializer(serializers.ModelSerializer):
    last_four = serializers.CharField(read_only=True)


    # def validate(self, attrs):
        
    #     expiry_date = dt.date(year=int(attrs['exp_year']), month=int(attrs['exp_month']) + 1, day=1)
    #     if dt.date.today() >= expiry_date:
    #         raise serializers.ValidationError('This card has expired')
        
    #     if len(attrs['card_number']) not in range(16, 20):
    #         raise serializers.ValidationError('Card Number should be between 16 and 19 digits')
        
    #     resp = Visa().validate_card(card_number=attrs['card_number'], cvv=attrs['cvv'], expiry_date="{}-{}".format(attrs['exp_year'], attrs['exp_month']))
    #     if not resp.ok:
    #         try:
    #             action_code = resp.json().get('actionCode')
    #             raise serializers.ValidationError(choices.visa_action_codes.get(action_code))
    #         except Exception as e:
    #             print("Visa API error:", e) #Todo: log error
            
    #     return super().validate(attrs)
    class Meta:
        model = Card
        fields = '__all__'
        write_only_fields = ['card_number', 'cvv']
        read_only_fields = ['auth_code','is_active','bank_name',]   

class TransactionSerializer(serializers.ModelSerializer):
    amount = MoneyField(max_digits=14, decimal_places=2, min_value=0)
    amount_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES)        
    amount_due = MoneyField(max_digits=14, decimal_places=2, min_value=0, required=False)
    amount_due_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES, required=False)
    balance_after_transaction = MoneyField(max_digits=14, decimal_places=2, min_value=0, read_only=True)
    balance_after_transaction_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES, read_only=True)
    fee = MoneyField(max_digits=14, decimal_places=2, min_value=0, required=False)
    fee_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES, required=False)
    short_reference = serializers.CharField(read_only=True)
    transaction_type = serializers.ChoiceField(choices=Transaction.TRANSACTION_TYPES)
    customer = CustomerSerializer(required=False)
    card = CardSerializer(write_only=True, required=False)
    
    class Meta:
        model = Transaction
        exclude = ['full_service_response']


    def create(self, validated_data):
        customer_data = validated_data.pop('customer', None)
        account = self.context.get('account')
        validated_data['account'] = account

        trans = super().create(validated_data)

        if customer_data:
            customer_data['account'] = account
            customer_instance, _ = Customer.objects.get_or_create(**customer_data)
            trans.customer = customer_instance

        return trans
    

class PaymentLinkTransactionSerializer(serializers.ModelSerializer):
    amount = MoneyField(max_digits=14, decimal_places=2, min_value=0)
    amount_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES)
    # fee = MoneyField(max_digits=14, decimal_places=2, min_value=0, required=False)
    # fee_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES, required=False)
    reference = serializers.CharField(read_only=True)
    transaction_type = serializers.ChoiceField(choices=Transaction.TRANSACTION_TYPES)
    customer = CustomerSerializer(required=True)
    card = CardSerializer(write_only=True, required=False)
    
    class Meta:
        model = Transaction
        fields = ['amount','amount_currency','transaction_type','customer','card','source_identifier','source',
            'description','meta','reference','source_reference','service','created_at','updated_at']

    def validate(self, attrs):
        attrs = super().validate(attrs) 
        amount = attrs.get('amount')
        amount_currency = attrs.get('amount_currency')
            
        if (attrs.get('transaction_type') == Transaction.CARD) and (attrs.get('service') is None):
            raise serializers.ValidationError('Card transactions must specify a service')
        
        if amount < Money(constants.TRF_MINIMUM[amount_currency], amount_currency):
            raise serializers.ValidationError('Minimum transaction amount is {}'.format(constants.TRF_MINIMUM[amount_currency]))

        if amount > Money(constants.TRF_LIMITS[amount_currency], amount_currency):
            raise serializers.ValidationError('Maximum transaction limit exceeded')
        
        if (attrs.get('transaction_type') == Transaction.CARD) and (attrs.get('card') is None):
            raise serializers.ValidationError('Card detail is required for card transactions')

        if card := attrs.get('card'):
            expiry_date = dt.date(year=int(card['exp_year']), month=int(card['exp_month']), day=1) + dt.timedelta(days=30)
            if dt.date.today() >= expiry_date:
                raise serializers.ValidationError('This card has expired')

            if not len(card['card_number']) in range(16, 20):
                raise serializers.ValidationError('Card Number should be between 16 and 19 digits')
            
            if not card['card_number'] in constants.TEST_CARDS:
                resp = Visa().validate_card(card_number=card['card_number'], cvv=card['cvv'], expiry_date="{}-{}".format(card['exp_year'], card['exp_month']))
                if resp.ok:
                    action_code = resp.json().get('actionCode')
                    if not action_code in ['00']:
                        raise serializers.ValidationError(choices.VISA_ACTION_CODES.get(action_code))
                        # raise serializers.ValidationError("Unable to validate this card")
                else:
                    error = resp.json().get('errorMessage')
                    print("Card Validation Error:", error)
                    raise serializers.ValidationError("Card validation failed")
        
        return attrs   


    def create(self, validated_data):
        customer_data = validated_data.pop('customer', None)
        card_data = validated_data.pop('card', None)
        account = self.context.get('account')
        request = self.context.get('request')

        validated_data['account'] = account
        validated_data['ip_address'] = utils.get_ip(request)

        with transaction.atomic():
            if card_data:
                card, _ = Card.objects.get_or_create(**card_data)
                validated_data['card'] = card

            if customer_instance := Customer.objects.filter(email=customer_data['email'], account=account).first():
                validated_data['customer'] = customer_instance
            else:
                customer_data['account'] = account
                validated_data['customer'] = Customer.objects.create(**customer_data)
            
            trans = super().create(validated_data)

        trans.succeed()
        return trans


class CheckoutTransactionSerializer(serializers.ModelSerializer):
    amount = MoneyField(max_digits=14, decimal_places=2, min_value=0)
    amount_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES)
    reference = serializers.CharField(read_only=True)
    amount_due = MoneyField(max_digits=14, decimal_places=2, min_value=0, required=False)
    amount_due_currency = serializers.ChoiceField(choices=settings.CURRENCY_CHOICES, required=False)
    transaction_type = serializers.ChoiceField(choices=Transaction.TRANSACTION_TYPES)
    card = CardSerializer(write_only=True, required=False)
    
    class Meta:
        model = Transaction
        fields = ['amount','amount_currency','transaction_type','customer','card','source','ip_address','service_reference',
            'description','meta','reference','source_reference','service','status','amount_due','amount_due_currency','created_at','updated_at']
        read_only_fields = ['account','ip_address','reference','service_reference','status','amount_due','amount_due_currency',
            'balance_after_transaction','fee','fee_currency']

    def validate(self, attrs):
        attrs = super().validate(attrs) 
        amount = attrs.get('amount')
        amount_currency = attrs.get('amount_currency')
       
        if amount.currency != self.context['account'].balance.currency:
            raise serializers.ValidationError("Provided currency does not match account's currency")
        
        if (attrs.get('transaction_type') == Transaction.CARD) and (attrs.get('service') is None):
            raise serializers.ValidationError('Card transactions must specify a service type')
        
        if amount < Money(constants.TRF_MINIMUM[amount_currency], amount_currency):
            raise serializers.ValidationError('Minimum transaction amount is {}'.format(constants.TRF_MINIMUM[amount_currency]))
        
        if amount > Money(constants.TRF_LIMITS[amount_currency], amount_currency):
            raise serializers.ValidationError('Maximum transaction limit exceeded')
        
        if (attrs.get('transaction_type') == Transaction.CARD) and (attrs.get('card') is None):
            raise serializers.ValidationError('Card detail is required for card transactions')
        
        if not (checkout := Checkout.objects.filter(code=attrs.get('source_reference')).first()):
            raise serializers.ValidationError('This "{}" checkout code was not found'.format(attrs.get('source_reference')))
        
        self.checkout = checkout

        if card := attrs.get('card'):
            expiry_date = dt.date(year=int(card['exp_year']), month=int(card['exp_month']), day=1) + dt.timedelta(days=30)
            if dt.date.today() >= expiry_date:
                raise serializers.ValidationError('This card has expired')

            if not len(card['card_number']) in range(16, 20):
                raise serializers.ValidationError('Card Number should be between 16 and 19 digits')
            
            # if card['card_number'] in constants.TEST_CARDS:
            if not card['card_number'] in constants.TEST_CARDS:
                resp = Visa().validate_card(card_number=card['card_number'], cvv=card['cvv'], expiry_date="{}-{}".format(card['exp_year'], card['exp_month']))
                if resp.ok:
                    action_code = resp.json().get('actionCode')
                    print('Card Validation Response:', resp.json())
                    if not action_code in ['00']:
                        raise serializers.ValidationError(choices.VISA_ACTION_CODES.get(action_code))
                        # raise serializers.ValidationError("Unable to validate this card")
                else:
                    error = resp.json().get('errorMessage')
                    print("Card Validation Error:", error)
                    raise serializers.ValidationError("Card validation failed")
        
        return attrs   


    def create(self, validated_data):
        card_data = validated_data.pop('card', None)
        account = self.context.get('account')
        request = self.context.get('request')

        validated_data['account'] = account
        validated_data['ip_address'] = utils.get_ip(request)

        with transaction.atomic():
            if card_data:
                card, _ = Card.objects.get_or_create(**card_data)
                validated_data['card'] = card
                validated_data['customer'] = self.checkout.customer
            
            trans = super().create(validated_data)

        trans.succeed()
        return trans