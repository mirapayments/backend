from django.urls import path

from payments import views


urlpatterns = [
    path('payment-link/<str:account_number>/', views.CreateListPaymentLinkView.as_view()),
    path('payment-link/<int:pk>/<str:account_number>/', views.RetrieveUpdateDeletePaymentLinkView.as_view()),
    path('payment-link/code/<str:code>/', views.CodeRetrievePaymentLink.as_view()),
    path('payment-link/toggle/<int:pk>/<str:account_number>/', views.TogglePaymentLink.as_view()),
    path('checkout/', views.CreateCheckout.as_view()),
    path('checkout/code/<str:code>/', views.RetrieveCheckout.as_view()),


]