from rest_framework import permissions

from users.models import Role
from accounts.models import Account


class IsOwnerOrAdmin(permissions.BasePermission):
    '''Permit only the Owner or Admin to perform an action'''

    message = 'You are not authorised to perform this action'

    def has_permission(self, request, view):
        
        role = Role.objects.filter(user=request.user, account__account_number=view.kwargs.get('account_number')).first()
        if role and (role.role in [Role.ADMIN, Role.OWNER]):
            return True
              
        return False


class IsAccountMember(permissions.BasePermission):
    '''Permit only memebers of the account to perform an action'''

    message = 'You are not a member of this account'

    def has_permission(self, request, view):
        account = Account.objects.filter(account_number=view.kwargs.get('account_number')).first()
        if account and (account in request.user.accounts.all()):
            return True

        return False    
