from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from djmoney.models.fields import MoneyField
from djmoney.models.validators import MinMoneyValidator
from djmoney.money import Money
from djmoney.contrib.exchange.models import convert_money

from helpers.utils import generate_unique_key, generate_unique_number
# from accounts.exceptions import InsufficientBalance


class Account(models.Model):
    '''
    The Live Account for handling user's payments information, and balance

    - public_key: stores the unique public key used for payments integration
    - account_type: the type of account user operates, see ACCOUNT_CHOICES below
    - balance: the account balance, managed with django-money
    - account_number: the account number, serves as account ID
    - name: the account name
    - is_active: whether account is suspended or active
    - setup_complete: if account is activated - and setup is complete, i.e compliance has been filled
    - live_webhook_url: live webhook url for this account
    - created_at: time account was created
    - updated_at: time account was updated
    '''

    INDIVIDUAL = 'Individual'
    COMPANY = 'Company'
    RELIGIOUS = 'Religious'
    GOVERNMENT = 'Government'
    NGO = 'NGO'
    ACCOUNT_CHOICES = (
        (INDIVIDUAL, INDIVIDUAL),
        (COMPANY, COMPANY),
        (RELIGIOUS, RELIGIOUS),
        (GOVERNMENT, GOVERNMENT),
        (NGO, NGO),
    )
    SENDER, RECEIVER = 'Sender', 'Receiver'
    FEES_PAYER_CHOICES = (
        (SENDER, SENDER),
        (RECEIVER, RECEIVER)
    )
    public_key = models.CharField(max_length=50, unique=True, editable=False)
    account_type = models.CharField(choices=ACCOUNT_CHOICES, max_length=100, default=INDIVIDUAL)
    balance = MoneyField(max_digits=14, decimal_places=2, default=0.0, validators=[MinMoneyValidator(0)])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    account_number = models.CharField(max_length=15, unique=True, editable=False)
    name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    setup_complete = models.BooleanField(default=False)
    live_webhook_url = models.URLField(blank=True, null=True)
    fees_payer = models.CharField(max_length=50, choices=FEES_PAYER_CHOICES, default=RECEIVER)
 

    def __str__(self):
        return "Live account: {}".format(self.account_number)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.account_number = generate_unique_number(klass=Account, field='account_number', len=10)
            self.public_key = generate_unique_key(klass=Account, field='public_key', len=40)
        
        self.full_clean()
        super().save(*args, **kwargs)

    @property
    def live_pk(self):
        return self.public_key    

    def sufficient_balance(self, amount: Money):
        '''Check if balance is sufficient'''

        return self.balance >= amount

    def valid_money(self, amount: Money):
        '''Check if money is valid to be added to account'''

        assert isinstance(amount, Money), 'Amount must be an instance of %s' %(Money.__name__)
        
        if self.balance.currency != amount.currency:
            return False
        return True
    
    def convert_money(self, amount: Money):
        '''Convert Money to appropriate currency'''

        if not self.valid_money(amount):
            converted_money = convert_money(value=amount, currency=self.balance_currency)
            # converted money might be overflow, round up
            amount = Money(round(converted_money.amount, 2), converted_money.currency)

            #TODO: log currency conversion for tracking
        return amount

    def credit(self, amount: Money):
        """ Credits a value to the account."""
    
        self.balance += self.convert_money(amount)
        self.save()
        return self

    def withdraw(self, amount):
        """
        Withdraw's a value from the wallet.
        Also creates a new transaction with the withdraw
        value.
        Should the withdrawn amount is greater than the
        balance this wallet currently has, it raises an
        :mod:`InsufficientBalance` error. This exception
        inherits from :mod:`django.db.IntegrityError`. So
        that it automatically rolls-back during a
        transaction lifecycle.
        """
        if not self.valid_money(amount):
            return

        if not self.sufficient_balance(amount):
            return

        self.transaction_set.create(
            value=-amount,
            current_balance=self.balance - amount
        )
        self.current_balance -= amount
        self.save()

    def transfer(self, dest, amount):
        pass


class TestAccount(models.Model):
    '''Test version of the user's account'''

    INDIVIDUAL = 'Individual'
    COMPANY = 'Company'
    RELIGIOUS = 'Religious'
    GOVERNMENT = 'Government'
    NGO = 'NGO'
    ACCOUNT_CHOICES = (
        (INDIVIDUAL, INDIVIDUAL),
        (COMPANY, COMPANY),
        (RELIGIOUS, RELIGIOUS),
        (GOVERNMENT, GOVERNMENT),
        (NGO, NGO),
    )
    public_key = models.CharField(max_length=50, unique=True, editable=False)
    account_type = models.CharField(choices=ACCOUNT_CHOICES, max_length=100)
    balance = MoneyField(max_digits=14, decimal_places=2, default_currency='NGN', default=0.0, validators=[MinMoneyValidator(0)])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    account_number = models.CharField(max_length=15, unique=True, editable=False)
    name = models.CharField(max_length=200)
    test_webhook_url = models.URLField(blank=True, null=True)
    setup_complete = models.BooleanField(default=False)

    def __str__(self):
        return "Test account: {}".format(self.account_number)

    @property
    def test_pk(self):
        return self.public_key    

    def sufficient_balance(self, amount):
        return self.balance.amount >= amount

    def valid_money(self, amount):
        if not isinstance(amount, Money):
           return False
        
        if self.balance.currency != amount.currency:
            return False
        return True    


    def deposit(self, amount):
        """
        Deposits a value to the account.
        Also creates a new transaction with the deposit value.
        """
        
        if self.valid_money(amount):

            self.transaction_set.create(
                amount=amount,
                current_balance=self.balance + amount
            )
            self.balance += amount
            self.save()
            return True
        return False    

    def withdraw(self, amount):
        """
        Withdraw's a value from the wallet.
        Also creates a new transaction with the withdraw
        value.
        Should the withdrawn amount is greater than the
        balance this wallet currently has, it raises an
        :mod:`InsufficientBalance` error. This exception
        inherits from :mod:`django.db.IntegrityError`. So
        that it automatically rolls-back during a
        transaction lifecycle.
        """
        if not self.valid_money(amount):
            return

        if not self.sufficient_balance(amount):
            return

        self.transaction_set.create(
            value=-amount,
            current_balance=self.balance - amount
        )
        self.current_balance -= amount
        self.save()

    def transfer(self, dest, amount):
        pass

    def save(self, *args, **kwargs):
        if not self.pk:
            self.public_key = generate_unique_key(TestAccount, 'public_key', 40)
        self.full_clean()
        super().save(*args, **kwargs)


class Compliance(models.Model):
    '''
    Store's user's Meta data and government info, serves as business/account KYC, to comply with
    regulatory rules and regulations

    - account: account who has this
    - contact_number: the contact number for the business/account
    - contact_email: the contact email for the business/account
    - official_address: the official address of the establishment
    - owener_name: the owner's name
    - nationality: nationality of the owner
    - identity_type: the preferred identity type for the owner
    - home_address: the home address of the owner
    - bank_acct_name: the payout bank account name
    - bank_number: the payout bank account number
    - bank_name: the payout bank name
    '''

    IDENTITY_CHOICES = (
        ('Drivers Licence', 'Drivers Licence'),
        ('International Passport', 'International Passport'),
        ('National Identity Card', 'National Identity Card'),
    )

    # account/biz info
    account = models.OneToOneField('accounts.Account', on_delete=models.CASCADE, null=True)
    contact_number = models.CharField(max_length=50, default='', blank=True)
    contact_email = models.EmailField(max_length=100, default='', blank=True)
    office_address = models.TextField(blank=True)

    # owner info
    owner_name = models.CharField(max_length=150, default='', blank=True)
    nationality = models.CharField(max_length=100, default='', blank=True)
    identity_type = models.CharField(max_length=150, default='', blank=True)
    identity_file = models.FileField(upload_to='compliance/', null=True, blank=True)
    home_address = models.TextField(blank=True)

    # bank account detail
    bank_acct_name = models.CharField(max_length=150, default='', blank=True)
    bank_number = models.CharField(max_length=15, default='', blank=True)
    bank_name = models.CharField(max_length=100, default='', blank=True)

    def __str__(self):
        return f"{self.account}'s Compliance"


# class AccountSetting(models.Model):
#     pass


# Create a test account, after a live account is created
@receiver(post_save, sender=Account)
def create_test_account(sender, instance, created, **kwargs):
    if created:
        TestAccount.objects.create(
            account_type=instance.account_type,
            balance=instance.balance,
            account_number=instance.account_number, 
            name=instance.name,
        )

# Delete the related test account after a live account is deleted - However, in the future, account
# deletion would be disabled      
@receiver(post_delete, sender=Account)
def delete_test_account(sender, instance, created, **kwargs):
    testaccount = TestAccount.objects.filter(account_number=instance.account_number)
    if testaccount.exists():
        testaccount.delete()
