messages = [
	(
		'You are a great man.', 
		'You are a blessing to us', 
		'Thank God for giving you to us', 
		'Thank you for answering the call of God'
	),
	(
		'Calvary greetings papa, in the name of Jesus Christ.',
		'I hope you are well and family is doing great?',
		'I pray for more grace and new dimensions.',
		'Thank you for all you do papa, We appreciate you'
	)
]

greetings = ['Good evening papa', 'Hello papa, good evening']