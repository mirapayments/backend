from rest_framework import serializers

from customers.models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    account = serializers.CharField(source='account.account_number',  read_only=True)
    phone = serializers.CharField(required=False)
    full_name = serializers.CharField(required=False)
    class Meta:
        model = Customer
        fields = '__all__'
        read_only_fields = ['blocked']
        
    def create(self, validated_data):
        account = self.context.get('account')
        validated_data['account'] = account
        customer, _ = Customer.objects.get_or_create(**validated_data)
        return customer
