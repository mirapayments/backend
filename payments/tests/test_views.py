from payments.tests.factories import PaymentLinkFactory
from rest_framework.test import APITestCase
from rest_framework import status

from users.tests.factories import UserFactory
from accounts.tests.factories import AccountFactory

class PaymentLinkTest(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.account = AccountFactory()
        self.user.accounts.add(self.account)
        self.payment_link = PaymentLinkFactory(account=self.account)

        
    def test_create_payment_link(self):
        '''Assert user can create payment links. '''

        data = {
            'name': 'new payment link',
            'description': 'to collect plenty money',
            'payment_type': 'Single',
            'amount_currency': 'USD',
            'amount': 2
        }
        url = f'/payments/payment-link/{self.account.account_number}/'

        self.client.force_authenticate(self.user)       
        resp = self.client.post(url, data) 

        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(resp.data['name'], data['name'])
        self.assertEqual(resp.data['amount_currency'], data['amount_currency'])
        self.assertEqual(resp.data['payment_type'], data['payment_type'])

    def test_list_payment_links(self):
        'Assert that listing all payments links for an account works'

        url = f'/payments/payment-link/{self.account.account_number}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.get(url) 
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_delete_payment_link(self):
        'Assert payment link can be deleted'

        url = f'/payments/payment-link/{self.payment_link.pk}/{self.account.account_number}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.delete(url) 
        
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)

    def test_retrieve_payment_link_w_code(self):
        '''Assert that payment link data can be retrieved using the auto generated code'''

        url = f'/payments/payment-link/code/{self.payment_link.code}/'
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue(resp.data['status'])
        self.assertEqual(self.payment_link.code, resp.data['data']['code'])


    def test_toggle_payment_link(self):
        '''Assert payment link status toggle works'''

        url = f'/payments/payment-link/toggle/{self.payment_link.id}/{self.account.account_number}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.post(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue(resp.data['status'])
        self.assertNotEqual(self.payment_link, resp.data['data']['is_active'])

    def test_retrieve_payment_link(self):
        '''Assert retrieve payment link  works'''

        url = f'/payments/payment-link/{self.payment_link.id}/{self.account.account_number}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(self.payment_link.is_active, resp.data['is_active'])

    def test_update_payment_link(self):
        '''Assert update payment link  works'''
        data = {
            'amount': 5000.0,
            'name': 'updated name',
            'amount_currency': 'NGN',
            'description': self.payment_link.description,
            'payment_type': self.payment_link.payment_type
        }
        url = f'/payments/payment-link/{self.payment_link.id}/{self.account.account_number}/'
        self.client.force_authenticate(self.user)   
        resp = self.client.put(url, data)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(data['name'], resp.data['name'])

