import factory
from djmoney.money import Money

from payments.models import PaymentLink


class PaymentLinkFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'payments.PaymentLink'

    payment_type = factory.Iterator([x[0] for x in PaymentLink.PAYMENT_CHOICES])
    amount =  Money(1000, 'NGN')
    name = factory.Sequence(lambda n: 'payment name {}'.format(n))
    description = factory.Sequence(lambda n: 'payment description {}'.format(n))
    account = factory.SubFactory('accounts.tests.factories.AccountFactory')