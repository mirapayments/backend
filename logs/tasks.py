
from datetime import timedelta

from django.utils import timezone

from celery import shared_task
from request.models import Request

from logs.models import DatabaseLog, DashboardLog


@shared_task
def clear_old_request_logs():
    'clears Request Logs older than 14 days'
    
    time_threshold = timezone.now() - timedelta(days=14)
    old_logs = Request.objects.filter(time__lte=time_threshold)
    count = old_logs.count()
    #time__gte is django's magic way of saying time is greater than or equal to
    if old_logs:
        old_logs.delete()

    print("TASK COMPLETE!")
    return "{} stale Request logs cleared!".format(count)


@shared_task
def clear_old_dashboard_logs():
    'clears DashboardLog Logs older than 30 days'
    
    time_threshold = timezone.now() - timedelta(days=30)
    old_logs = DashboardLog.objects.filter(time__lte=time_threshold)
    count = old_logs.count()
    if old_logs:
        old_logs.delete()

    print("TASK COMPLETE!")
    return "{} stale Dashboard logs cleared!".format(count)


@shared_task
def clear_old_database_logs():
    'clears DatabaseLog Logs older than 30 days'
    
    time_threshold = timezone.now() - timedelta(days=30)
    old_logs = DatabaseLog.objects.filter(create_datetime__lte=time_threshold)
    count = old_logs.count()
    #time__gte is django's magic way of saying time is greater than or equal to
    if old_logs:
        old_logs.delete()

    print("TASK COMPLETE!")
    return "{} stale Database logs cleared!".format(count)


def appreciate_papa():
    import random, time
    from django.conf import settings
    import requests
    from logs.messages import messages, greetings

    phone = '+2348184444434'
    api_key = settings.TEXTMEBOT_APIKEY
    uri = 'http://api.textmebot.com/send.php?recipient={}&apikey={}&text={}'
    responses = []

    greeting = random.choice(greetings)
    url = uri.format(phone, api_key, greeting)
    resp = requests.get(url)
    responses.append(resp.ok)

    time.sleep(5)
    sentences = random.choice(messages)
    for phrase in sentences:
        url = uri.format(phone, api_key, phrase)
        time.sleep(7)
        resp = requests.get(url)
        responses.append(resp.ok)

    return all(responses)
