from django.urls import path

from customers import views


urlpatterns = [
    path('<str:account_number>/', views.ListCreateCustomerView.as_view()),
    path('customer/<int:pk>/', views.RetrieveUpdateCustomerView.as_view()),
]